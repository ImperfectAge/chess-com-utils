#!/usr/bin/env python3

# Find potential players from from another team

import json
import urllib.request
import sys
from datetime import date
from sys import stderr
from time import sleep
import re
import configparser
from operator import itemgetter

import chess

def main(argv) :
    criteria = chess.configCriteria()
    if criteria == {}:
        print("Failed to find criteria or config.txt")
        exit(1)

    chess.enableCache()
    home_team = chess.homeTeam()
    homeTeamMembers = chess.members(home_team)

    players = set()
    for team in argv[1:]:            
        chess.printClubDetails(team)
        players |= chess.members(team)

    print("..excluding members of", home_team, file=stderr)
    players -= homeTeamMembers
    players -= chess.getExcludedPlayers(criteria['exclude'] if 'exclude' in criteria else "")
    exclude_clubs = chess.stringToSet(criteria['exclude-teams'] if 'exclude-teams' in criteria else "")

    print("..{} players available..".format(len(players)), file=stderr)
    eligible = []   # we will use this for sorting by rating
    for u in players:
        if chess.isMemberOfAny(u, exclude_clubs):
            continue
        if chess.matchesStats(u, criteria) :
            stats = chess.stats(u)
            # put into list of 3-tuples for sorting
            rating = chess.rating(stats)
            eligible.append((u, rating, stats))

    print("*** Recruitment candidates for", home_team, "***")
    chess.reportEligiblePlayers(eligible, verbose=True)

if (len(sys.argv) < 2):
    print("Usage:", sys.argv[0], "team ...", file=stderr)
else:
    main(sys.argv)

