#!/usr/bin/env python3

import os
import sys
from datetime import datetime
from sys import stderr
import importlib
import csv
import time
from pick import pick

import chess
multiplayer = importlib.import_module("multi-club-players")

verbose = False


class Team:
    def __init__(self, teamid, div):
        details = chess.clubDetails(teamid)
        self.name = details['name'] if details else teamid
        self.id = teamid
        self.div = div
        self.matches = []

homecfg = None

def createLeagueData(dir = None):
    global roundInDays, start, before, leaguetz, pattern, cfgDivisions, cfgParameters, teamToAbbrev, allTeams, divisions, divteams, homecfg
    config=os.path.join(dir, "config.txt") if dir else "config.txt"
    cfg = chess.readConfig(fname=config)
    if 'home' not in cfg:
        print(f"league.py: Could not find [home] section in {config}")
        sys.exit(3)
    homecfg = cfg['home']

    roundInDays = 30.4        # length of a round in the league

    start = '2020-07-01'
    if 'start' in homecfg:
        start = homecfg['start']
    before = None
    if 'end' in homecfg:
        before = homecfg['end']
    if 'round' in homecfg:
        roundstr = homecfg['round'].split()
        period = roundstr[1].lower()
        if period == 'week' or period == 'weeks':
            roundInDays = 7 * float(roundstr[0])
        elif period == 'month' or period == 'months':
            roundInDays = 365.0 * float(roundstr[0]) / 12.0     # take the average length of a month - approx 30.4 days
        elif period == 'day' or period == 'days':
            roundInDays = float(roundstr[0])

    leaguetz = homecfg.get('timezone', 'UTC')
    pattern = homecfg.get('pattern', "")

    if 'divisions' not in cfg:
        print("Could not find [divisions] section")
        sys.exit(5)
    cfgDivisions = cfg['divisions']

    if 'parameters' not in cfg:
        print("Could not find [parameters] section")
        cfgParameters = {}
    else:
        cfgParameters = cfg['parameters']

    divteams = {}           # team names in each division
    for div in cfgDivisions:
        divteams[div] = [x.strip() for x in cfgDivisions[div].split(',') if x.strip()]
    allTeams = []       # list of all Teams
    divisions = {}      # list of Teams in each division
    # map player to list of teams (Team object) they played for

    # Create the team objects, which includes the current membership of that club
    for div in cfgDivisions:
        teams = divteams[div]
        teamobjs = []
        for teamid in teams:
            team = Team(teamid, div)
            allTeams.append(team)
            teamobjs.append(team)
        divisions[div] = teamobjs

    # read the CSV file abbrevs.csv or state-clubs.csv.  This maps all team IDs to abbrevs
    try:
        state_clubs=os.path.join(dir, "state-clubs.csv") if dir else "state-clubs.csv"
        teamToAbbrev = chess.readClubAbbrevs(state_clubs)
    except:
        try:
            abbrevs=os.path.join(dir, "abbrevs.csv") if dir else "abbrevs.csv"
            teamToAbbrev = chess.readClubAbbrevs(abbrevs)
        except:
            print(f'No abbreviation mapping file found in dir {dir}', file=stderr)
            teamToAbbrev = {}

#############################################################

def findTeamMatches(start, before, registration, running, completed, fpv, pattern):
    if before:
        print("Looking for matches starting after", start, 'and before', before)
    else:
        print("Looking for matches starting after", start)
    startDate = int(datetime.strptime(start, '%Y-%m-%d').timestamp())
    beforeDate = 0
    if before:
        beforeDate = int(datetime.strptime(before, '%Y-%m-%d').timestamp())
    states = []
    if registration:
        states.append("registered")
    if completed or fpv:
        states.append("finished")
    if running or fpv:
        states.append("in_progress")
    # First we find the matches of interest for each team. These are the current season and matches against opponents in same division
    for team in allTeams:
        team.matches += chess.clubMatchList(team.id, states, start=startDate, before=beforeDate, opponents=divteams[team.div], pattern=pattern)
        # Matches is a list of tuples (matchid, json)
        team.matches.sort(key=lambda m: m[1].get('start_time',0) )

def reportMatches(boards, registration):
    # Now for each team, and each match, look at the players from those matches
    leagueMatches = set()
    for div in divisions:
        print("** DIVISION ", div)
        for team in divisions[div]:
            if verbose:
                print("Team: ", team.name, team.id, 'Div ' + team.div,
                      str(len(team.matches)) + ' matches', sep='\t')
            if registration and not team.matches:
                print("*", team.name, "does not have a match in registration.")
            for match, json in team.matches:
                if match not in leagueMatches:
                    leagueMatches.add(match)
                    # If boards is set, we don't want to double-report
                    chess.reportClubMatch(json, team.id, verbose and not boards, tz=leaguetz, warnings=False)
                    if boards:
                        chess.printMatch(match, boards=True, tz=leaguetz)
                        print()
    if verbose :
        print()

def clubsByDivision():
    clubs = {}
    for div in cfgDivisions:
        divclubs = []
        for team in divisions[div]:
            name = team.name
            url = chess.teamUrl(team.id)
            abbr = state(team.id)
            id = team.id
            divclubs.append({'name' : name, 'url' : url, 'abbr' : abbr, 'id': id })
        clubs[div] = divclubs
    return clubs

def detectFPV():
    boards = False
    if not chess.isRefreshed():
        print("Warning - using cached results, use -refresh for latest status")
    # Now for each team, and each match, look at the players from those matches and calculate any penalties
    matchesSeen = set()
    for team in allTeams:
        for match, json in team.matches:
            # make sure we don't calculate each match twice!
            if match not in matchesSeen:
                matchesSeen.add(match)
                closedPlayerPenalty(team.id, match, verbose)


def detectMultiplayers():
    multiplayer.detectMatchMultiplayers(allTeams, divisions)


def reportPenalties():
    # Finally, we report all the calculated penalties (may include MP and FPV)
    matchesSeen = set()
    division = None
    for team in allTeams:
        if verbose:
            if team.div != division:
                division = team.div
                print("** DIVISION ", division)
                print()
            print("Team: ", team.name, team.id, 'Div ' + team.div,
                  str(len(team.matches)) + ' matches', sep='\t')
        for match, json in team.matches:
            # Don't report each match twice
            if match not in matchesSeen:
                matchesSeen.add(match)
                penalty_applied = getCalculatedPenalty(team.id, match)
                # Ignore zero penalties
                if penalty_applied != 0:
                    if team.div != division:
                        division = team.div
                        print("** DIVISION ", division)
                        print()
                    reportCalculatedPenalty(team.id, match, penalty_applied, verbose)
    if verbose:
        print()


def closedPlayerPenalty(teamId, match, verbose):
    # Calculate penalty to be applied to match if player is DQ'd
    matchDetails = chess.match_details(match)
    if not matchDetails:
        print(f"Match {match} has no teams!", file=stderr)
        print(matchDetails, file=stderr)
        return
    teams = matchDetails["teams"]
    # We are going through both teams here.. need to ensure we only eval each match once.
    for team in ("team1", "team2"):
        teamj = teams[team]
        # List of players closed for FPV (doesn't include players closed after match finished!)
        fp_removals = [u.lower() for u in teamj["fair_play_removals"]]
        penalty_applied = 0
        anyClosed = False
        for u in teamj["players"]:
            # Look for players with FPV closed account (current account status)
            player = u["username"].lower()
            if player in fp_removals or 'fair_play' in chess.playerStatus(player):
                anyClosed = True
                if 'fpv' not in teamj:
                    teamj['fpv'] = set()
                teamj['fpv'].add(player)
                if 'board' in u:
                    board = chess.match_board(u['board'])
                    score = board['board_scores'][player]
                    penalty_applied += score
        if anyClosed:
            teamj['penalty'] = teamj.get('penalty', 0) + penalty_applied

def getCalculatedPenalty(teamId, match):
    # Ignore zero penalties
    matchDetails = chess.match_details(match)
    if not matchDetails:
        return 0
    teams = matchDetails["teams"]
    team1ID = chess.teamId(teams["team1"]["url"])
    home = "team1" if team1ID == teamId else "team2"
    away = "team2" if team1ID == teamId else "team1"
    # extract penalty from both teams!
    penalty_applied = teams[home].get('penalty', 0) - teams[away].get('penalty', 0)
    return penalty_applied

def reportCalculatedPenalty(teamId, match, penalty_applied, verbose):
    matchDetails = chess.match_details(match)
    if not matchDetails:
        return
    teams = matchDetails["teams"]
    team1ID = chess.teamId(teams["team1"]["url"])
    home = "team1" if team1ID == teamId else "team2"
    away = "team2" if team1ID == teamId else "team1"

    homescore = teams[home]["score"]
    awayscore = teams[away]["score"]
    if verbose:
        chess.printMatch(match, matchJson=matchDetails, compact=True, tz=leaguetz)
    else:
        print(match, matchDetails["name"], matchDetails['url'])
    for team in (home, away):
        teamj = teams[team]
        for player in teamj.get('fpv', set()):
            print(f"{teamj['name']} player @{player} - {chess.playerStatus(player)}")
        for player in teamj.get('multiplayer', []):
            print(f"{teamj['name']} player @{player} - Multiplayer")
    reportMatchPenalty(teams[home]["name"], homescore, teams[away]["name"], awayscore, penalty_applied)
    teams[home]["score"] = homescore - penalty_applied
    teams[away]["score"] = awayscore + penalty_applied


def reportMatchPenalty(homename, homescore, oppsname, oppscore, penalty_applied):
    if penalty_applied == 0:
        print("         No Adjustment:", homename, str(homescore) + ",", oppsname, oppscore)
    else:
        newhomescore  = homescore - penalty_applied
        newoppscore   = oppscore  + penalty_applied
        changedResult = (newhomescore == newoppscore and homescore != oppscore) or \
                        (newhomescore > newoppscore and homescore <= oppscore) or \
                        (newhomescore < newoppscore and homescore >= oppscore)
        print(f"         Adjustment: {homename} {homescore}, {oppsname} {oppscore}\t=>\t{homename} {newhomescore}, {oppsname} {newoppscore} {'CHANGED RESULT' if changedResult else ''}")
    print()


def keyInTupleList(key, tlist):
    # is the specified key the first element of one of the tuples in the list tlist?
    for k, v in tlist:
        if k == key:
            return True
    return False

# This will find current matches from the API and also any additional matches or records from the saved scores_file
def getCurrentMatchScores(verifyBoards, table, reportForfeits, scores_file):
    currentMatchScores = []
    seenMatches = set()
    for div in divisions:
        # for each team, and each match, check the scores in the match
        # Matches will be a list of tuples (matchid, json).
        matches = list()
        for team in divisions[div]:
            if verbose:
                print("Team: ", team.name, team.id, 'Div ' + team.div,
                      str(len(team.matches)) + ' matches', sep='\t')
            for match, json in team.matches:
                # Make sure the match is not in the list already
                if not keyInTupleList(match, matches):
                    matches.append((match, json))

        # What do we do when the same pair of clubs appears twice?
        # If one match doesn't have the correct parameters, throw it out, otherwise accept the earlier one
        seenPairings = {}
        removeMatches = set()
        for match, json in matches:
            t1, t2 = getMatchTeams(match, json)
            if (t1, t2) in seenPairings:
                prevMatch = seenPairings[(t1,t2)]
                print(f"Duplicate pairings between {t1} and {t2}: {prevMatch} and {match}")
                # TODO If one match doesn't have the correct parameters, throw it out, otherwise accept the earlier one
                badParams = verifyMatchParameters(div, prevMatch)
                badParams2 = verifyMatchParameters(div, match)
                if badParams:
                    print(f'  ..Removing {prevMatch} with errors:', '\n  - '.join(badParams))
                    removeMatches.add(prevMatch)
                elif badParams2:
                    print(f'  ..Removing {match} with errors:', '\n  - '.join(badParams2))
                    removeMatches.add(match)
                elif prevMatch < match:
                    print(f'  ..Removing later match {match} ')
                    removeMatches.add(match)
                else:
                    print(f'  ..Removing later match {prevMatch} ')
                    removeMatches.add(prevMatch)
            seenPairings[(t1,t2)] = match

        # Verify the scores and update the matchDetails json if necessary
        for match, json in matches:
            if match not in removeMatches:
                verifyMinimumPlayers(match, reportForfeits)
        if verifyBoards:
            # This one does go through board-by-board
            for match, json in matches:
                if match not in removeMatches:
                    verifyMatchScore(div, match)
        for match, json in matches:
            if match not in removeMatches:
                scoreDesc = createMatchScore(div, match, table)
                seenMatches.add(match)
                currentMatchScores.append(scoreDesc)
    # Read the saved scores - add any additional matches or recorded defaults back into the current scores
    for matchid, score in readScores(scores_file).items():
        if matchid not in seenMatches:
            currentMatchScores.append(score)
    # sort by division and then start time
    currentMatchScores.sort(key=lambda m: (m.get('division','ZZZ'), int(m.get('start', 99999999999))))
    return currentMatchScores

def getMatchTeams(match, json):
    matchDetails = chess.match_details(match)
    if not matchDetails:
        return None, None
    teams = matchDetails["teams"]
    t1 = chess.teamId(teams["team1"]["url"])
    t2 = chess.teamId(teams["team2"]["url"])
    if t2 < t1:
        return t2, t1
    else:
        return t1, t2

def verifyMinimumPlayers(match, report):
    matchDetails = chess.match_details(match)
    if not matchDetails:
        return
    min_players = matchDetails.get("settings",{}).get("min_team_players", 0)
    teams = matchDetails["teams"]
    url = matchDetails["url"]
    home = "team1"
    away = "team2"
    # Check if either team failed to meet the min number of players.  We update the score in this case
    if matchDetails["status"] != "registration" and int(matchDetails['boards']) < min_players:
        name = matchDetails['name']
        teams[home]["score"] = 0
        teams[away]["score"] = 0
        if len(teams[home]["players"]) < min_players and len(teams[away]["players"]) < min_players:
            if report:
                print(f'   "{name}" - Double forfeit - both teams failed to reach min number of players - {url}')
        elif len(teams[home]["players"]) < min_players:
            if report:
                print(f'   "{name}" - {teams[home]["name"]} failed to reach min number of players - {url}')
            teams[away]["score"] = min_players*2
        elif len(teams[away]["players"]) < min_players:
            if report:
                print(f'   "{name}" - {teams[away]["name"]} failed to reach min number of players - {url}')
            teams[home]["score"] = min_players*2

# Total the match score and ensure it matches the declared score.  Updates cached matchDetails json!
def verifyMatchScore(div, match, save=False, compare=False, scores_file=None):
    matchDetails = chess.match_details(match)
    if not matchDetails:
        return
    min_players = matchDetails.get("settings",{}).get("min_team_players", 0)
    teams = matchDetails["teams"]
    url = matchDetails["url"]
    home = "team1"
    away = "team2"

    # First check if either team failed to meet the min number of players.  We already updated the score in this case, no need to go through boards
    if matchDetails["status"] != "registration" and int(matchDetails['boards']) < min_players:
        return

    # Look through the boards totalling scores. 
    homescore_calc = 0
    oppsscore_calc = 0
    for u in teams[home]["players"]:
        player = u["username"].lower()
        if 'board' in u:
            board = chess.match_board(u['board'])
            score = board['board_scores'][player]
            homescore_calc += score
    for u in teams[away]["players"]:
        player = u["username"].lower()
        if 'board' in u:
            board = chess.match_board(u['board'])
            score = board['board_scores'][player]
            oppsscore_calc += score

    # Apply any calculated penalty directly (FPV or Multiplayer)
    penalty_applied = teams[home].get('penalty', 0) - teams[away].get('penalty', 0)
    homescore_calc -= penalty_applied
    oppsscore_calc += penalty_applied

    # check if either team's calculated score is different than the reported score
    if teams[home]["score"] != homescore_calc or teams[away]["score"] != oppsscore_calc:
        print(f"** Calculated score differs - {matchReport(createMatchScore(div, match))} (penalty {penalty_applied}) - {url}")
        print(f"{teams[home]['name']} score reported: {teams[home]['score']}, calculated: {homescore_calc} penalty: {teams[home].get('penalty', 0)}")
        print(f"{teams[away]['name']} score reported: {teams[away]['score']}, calculated: {oppsscore_calc} penalty :{teams[away].get('penalty', 0)}")
        # If we save the calculated score here we might be double-counting FPV/MP penalties
        #teams[home]["score"] = homescore_calc
        #teams[away]["score"] = oppsscore_calc

def matchReport(result, comma=False, table=False):
    l = []
    # Convert to a list per the csv_header, unless table is True then use the results_table_header
    for key in results_table_header() if table else csv_header():
        l.append(result.get(key, ""))
    return listAsString(l, comma=comma)

def listAsString(resultList, comma=False):
    sep = ", " if comma else " "
    return sep.join(map(str, resultList))

def saveMatch(result, scores_file):
    header = result.keys()
    with open(scores_file, 'r', newline='') as fd:
        reader = csv.reader(fd)
        # Just read the first row, which is the CSV header
        it = iter(reader)
        header = next(it, None)
    with open(scores_file, 'a', newline='') as fd:
        dictwriter = csv.DictWriter(fd, fieldnames=header)
        dictwriter.writerow(result)

def saveScores(currentMatchScores, scores_file):
    with open(scores_file, 'w', newline='') as fd:
        # Write all fields
        fields = csv_header()
        for f in results_table_header():
            if f not in fields:
                fields.append(f)
        for match in currentMatchScores:
            for f in match.keys():
                if f not in fields:
                    fields.append(f)
        dictwriter = csv.DictWriter(fd, fieldnames=fields)
        dictwriter.writeheader()
        dictwriter.writerows(currentMatchScores)

def readScores(scores_file):
    # Just read the saved scores
    savedScores = {}
    with open(scores_file, 'r', newline='') as fd:
        reader = csv.DictReader(fd)
        for row in reader:
            matchid = chess.matchId(row['url'])
            savedScores[matchid] = row
    return savedScores

def compareScores(currentMatchScores, scores_file):
    # Run comparison and report only changes to the result
    savedScores = readScores(scores_file)
    # For each result in currentMatchScores, compare the result with the result from saved score.
    for result in currentMatchScores:
        matchid = chess.matchId(result['url'])      # URL is second element
        if not matchid or matchid not in savedScores:
            print(f"Found:\t{matchReport(result)}")
        else:
            # compare result of saved match values in savedScores with that in resultList
            savedMatch = savedScores[matchid]
            if savedMatch['result'] != result['result']:
                print(f"Update:\t{matchReport(result)}")

def reportScores(currentMatchScores):
    # We just report the matches.  By division
    for div in divisions:
        print("** DIVISION ", div)
        for scoreDesc in currentMatchScores:
            if scoreDesc['division'] == div:
                print(matchReport(scoreDesc))

def reportDivisionTables(currentMatchScores, matchResults=False, printUrl=False, calcSbScore=True):
    for div in divisions:
        print("\n** DIVISION ", div)
        divisionResults = []
        for scoreDesc in currentMatchScores:
            if scoreDesc['division'] == div:
                divisionResults.append(scoreDesc)
        printDivisionTable(divisionResults, matchResults=matchResults, printUrl=printUrl, calcSbScore=calcSbScore)

def printDivisionTable(results, matchResults=False, printUrl=False, calcSbScore=True):
    # we are just dealing with one division.
    # each item in results is a dict with keys defined by results_table_header
    HOME='home'
    AWAY='away'
    HOMESCORE='homescore'
    HOMERESULT='homematch'
    AWAYSCORE='awayscore'
    AWAYRESULT='awaymatch'
    RESULT='result'
    START='start'
    URL='url'
    scores = {}
    played = {}
    sbscore = {}
    # calculate the overall score for each state (win=1, draw=0.5)
    for match in results:
        home = match[HOME]
        away = match[AWAY]
        scores[home] = scores.get(home, 0) + float(match[HOMERESULT])
        scores[away] = scores.get(away, 0) + float(match[AWAYRESULT])
        if match[RESULT] != "Playing":
            played[home] = played.get(home, 0) + 1
            played[away] = played.get(away, 0) + 1
    # Calculate the Sonneborn–Berger score for tiebreaks.
    # For each teeam, we sum the scores of each defeated opponent, plus half the score of each drawn opponenent 
    for match in results:
        home = match[HOME]
        away = match[AWAY]
        # Add the scores of each defeated opponent, plus half the score of each drawn opponenent 
        sbscore[home] = sbscore.get(home, 0) + (scores[away] * float(match[HOMERESULT]))
        sbscore[away] = sbscore.get(away, 0) + (scores[home] * float(match[AWAYRESULT]))
    # sort by score (highest first) and matches played as secondary key, and state name as third key
    sortedClubs = sorted(scores.items(), key=lambda kv: (-kv[1], played.get(kv[0], 0), kv[0]))
    prefix = homecfg.get('roundprefix', 'R')
    # ideally we should generate a table - CSV or HTML here.  But for now we display as 
    for kv in sortedClubs:
        club = kv[0]
        clubscore = kv[1]
        if int(clubscore) == clubscore:     # change 1.0 to 1
          clubscore = int(clubscore) 
        sb = sbscore.get(club, 0)
        if int(sb) == sb:
          sb = int(sb)
        print(f"{club}: Win Score {clubscore}/{played.get(club, 0)}  S-B: {sb}")
        if matchResults:
            for match in results:
                if match[HOME] == club:
                    roundNum = startToRound(match[START])
                    if printUrl:
                        print(f"  {prefix}{roundNum} {match[AWAY]} -> {match[URL]} {match[RESULT]} \t{match[HOMESCORE]}-{match[AWAYSCORE]}")
                    else:
                        print(f"  {prefix}{roundNum} {match[AWAY]} -> {match[RESULT]} \t{match[HOMESCORE]}-{match[AWAYSCORE]}")
                elif match[AWAY] == club:
                    roundNum = startToRound(match[START])
                    if printUrl:
                        print(f"  {prefix}{roundNum} {match[HOME]} -> {match[URL]} {match[RESULT]} \t{match[AWAYSCORE]}-{match[HOMESCORE]}")
                    else:
                        print(f"  {prefix}{roundNum} {match[HOME]} -> {match[RESULT]} \t{match[AWAYSCORE]}-{match[HOMESCORE]}")

# get the round number based on the start date (and the declared length of each round)
def startToRound(start):
    leagueStart = homecfg['start']
    leagueStartDate = int(datetime.strptime(leagueStart, '%Y-%m-%d').timestamp())
    numSecs = int(start) - leagueStartDate
    numDays = (numSecs / 3600) / 24
    roundNumber = int(numDays // roundInDays)
    roundNumber += 1
    return roundNumber

def checkScores(verifyBoards, save, compare, table, scores_file, readOnly=False, reportForfeits=False, checkParameters=True):
    if readOnly:
        print('Reporting match scores saved in', scores_file)
        currentMatchScores = readScores(scores_file).values()
    else:
        if verifyBoards:
            print('Checking current scores board-by-board...')
        else:
            print('Finding current match scores...')
        currentMatchScores = getCurrentMatchScores(verifyBoards, table, reportForfeits, scores_file)

    if checkParameters:
        if not cfgParameters:
            print("[parameters] section not found")
        else:
            checkAllMatchParameters(currentMatchScores)

    if not readOnly:
        if compare:
            print('Comparing current scores with those saved in', scores_file)
            compareScores(currentMatchScores, scores_file)
        if save:
            saveScores(currentMatchScores, scores_file)
            print('Saved current scores in', scores_file)

    if table:
        reportDivisionTables(currentMatchScores, matchResults=True, printUrl=False, calcSbScore=True)
    if not compare and not save and not table and not checkParameters:
        reportScores(currentMatchScores)
    if verbose:
        print()


# Anacronistic name.  This is the format of the simple report from '--compare' option
def csv_header():
    return ['division', 'round', 'name', 'url', 'boards', 'home', 'homescore', 'away', 'awayscore', 'result']

# following header is used when table=true
def results_table_header():
    # start date is used here as proxy for round number
    return ['division', 'start', 'home', 'homescore', 'homematch', 'away', 'awayscore', 'awaymatch', 'url', 'result']

def createMatchScore(division, match, table=False):
    matchDetails = chess.match_details(match)
    if not matchDetails:
        return {}
    score = {}
    teams = matchDetails["teams"]
    min_players = matchDetails.get("settings",{}).get("min_team_players", 0)
    home = "team1"
    away = "team2"
    homescore = teams[home]["score"]
    awayscore = teams[away]["score"]
    matchResultHome = 0
    matchResultAway = 0

    if matchDetails["status"] == "registration":
        result = "Registration"
    # check if either team's failed to meet the min number of players
    elif int(matchDetails['boards']) < min_players:
        if len(teams[home]["players"]) < min_players and len(teams[away]["players"]) < min_players:
            result = 'Double forfeit - both teams insufficient num players'
        elif len(teams[home]["players"]) < min_players:
            matchResultAway = 1
            result = stateJ(teams[away]) if table else f"Winner: {teams[away]['name']} ({teams[home]['name']} insufficient num players)"
            awayscore = min_players*2
        elif len(teams[away]["players"]) < min_players:
            matchResultHome = 1
            result = stateJ(teams[home]) if table else f"Winner: {teams[home]['name']} ({teams[away]['name']} insufficient num players)"
            homescore = min_players*2
    elif homescore > matchDetails["boards"]:
        matchResultHome = 1
        result = stateJ(teams[home]) if table else f"Winner: {teams[home]['name']}"
    elif awayscore > matchDetails["boards"]:
        matchResultAway = 1
        result = stateJ(teams[away]) if table else f"Winner: {teams[away]['name']}"
    elif homescore == matchDetails["boards"] and awayscore == matchDetails["boards"]:
        matchResultHome = 0.5
        matchResultAway = 0.5
        result = "Draw"
    else:
        result = "Playing"

    score['division'] = division
    match_start = matchDetails.get('start_time', 0)
    score['start'] = int(match_start)
    prefix = homecfg.get('roundprefix', 'R')
    score['round'] = f'{prefix}{startToRound(match_start)}'
    score['name'] = matchDetails["name"].strip()
    score['url'] = matchDetails["url"]
    score['boards'] = matchDetails["boards"]
    score['home'] = teams[home]['name'] 
    score['homeid'] = chess.teamId(teams[home]['@id'])
    score['homescore'] = int(homescore) if int(homescore) == homescore else homescore
    score['homematch'] = matchResultHome
    score['away'] = teams[away]['name']
    score['awayid'] = chess.teamId(teams[away]['@id'])
    score['awayscore'] = int(awayscore) if int(awayscore) == awayscore else awayscore
    score['awaymatch'] = matchResultAway
    score['result'] = result
    return score

def findDivisionFromMatch(matchDetails):
    teams = matchDetails["teams"]
    team1 = chess.teamId(teams['team1']['@id'])
    team2 = chess.teamId(teams['team2']['@id'])
    for division in divisions:
        clubs = set([team.id for team in divisions[division] ])
        if team1 in clubs and team2 in clubs:
            return division
    return None

def inputMatch():
    matchDetails = None
    YESNO = ["No", "Yes"]
    ok = False

    while not ok:
        url = input("URL or Match ID: ").strip()
        matchDetails = chess.match_details(url)
        if matchDetails:
            ok = True
        else:
            resp, _ = pick(YESNO, title=f"No match found at {url}.  Re-enter?", default_index=0)
            ok = resp == "No"

    # If we have the match details we should be able to calculate division from the 2 teams
    if matchDetails:
        division = findDivisionFromMatch(matchDetails)
        if not division:
            print("Clubs in match are not in any division!")
        return createMatchScore(division, url)

    division, _ = pick(list(divisions.keys()), title="Division")
    clubs = [team.name for team in divisions[division] ]
    team1, team1idx = pick(clubs, "Team 1")
    team2, team2idx = pick(clubs, "Team 2")
    homeTeam = divisions[division][team1idx]
    awayTeam = divisions[division][team2idx]

    winner, winnerIdx = pick([team1, team2, "Neither (double forfeit)"], "Which team is the winner?")
    result = input(f"Describe {homeTeam.name} vs {awayTeam.name} result: ").strip()

    divIndex = divToIndex(division)
    min_players = divParam(divIndex, cfgParameters.get("min_players", 0))

    matchResultHome = 1 if winnerIdx == 0 else 0
    homescore = (min_players*2) if winnerIdx == 0 else 0
    matchResultAway = 1 if winnerIdx == 1 else 0
    awayscore = (min_players*2) if winnerIdx == 1 else 0

    score = {}
    score['division'] = division
    match_start = int(time.time())
    score['start'] = int(match_start)
    prefix = homecfg.get('roundprefix', 'R')
    score['round'] = f'{prefix}{startToRound(match_start)}'
    score['name'] = f"{homeTeam.name} vs {awayTeam.name}"
    score['url'] = url
    score['boards'] = min_players
    score['home'] = homeTeam.name 
    score['homeid'] = homeTeam.id
    score['homescore'] = homescore
    score['homematch'] = matchResultHome
    score['away'] = awayTeam.name
    score['awayid'] = awayTeam.id
    score['awayscore'] = awayscore
    score['awaymatch'] = matchResultAway
    score['result'] = result
    print(score)
    return score

def checkAllMatchParameters(currentMatchScores):
    seen = set()
    for div in divisions:
        for scoreDesc in currentMatchScores:
            if scoreDesc['division'] == div:
                matchId = chess.matchId(scoreDesc['url'])    
                if matchId not in seen:
                    seen.add(matchId)
                    errors = verifyMatchParameters(div, matchId)
                    if errors:
                        print(matchReport(scoreDesc))
                        print('  -', '\n  - '.join(errors))

# Split the parameter based on the 'index' (order) of the division
def divParam(divIndex, parameter):
    if ',' in parameter:
        value = parameter.split(',')[divIndex]
    else:
        value = parameter
    return int(value.strip())

# Map time control to days per move
timeControl = { "1/86400" : 1 , "1/172800" : 2, "1/259200" : 3, "1/432000" : 5, "1/604800" : 7, "1/864000" : 10}

def verifyMatchParameters(div, matchId):
    details = chess.match_details(matchId)
    if 'settings' not in details: 
        return []       # No match

    divIndex = divToIndex(div)
    min_players = divParam(divIndex, cfgParameters.get("min_players", 0))
    max_players = divParam(divIndex, cfgParameters.get("max_players", 0))
    min_number_games = divParam(divIndex, cfgParameters.get("min_number_games", 0))
    rules = cfgParameters.get("rules", "chess")
    days_per_move = divParam(divIndex, cfgParameters.get("days_per_move", 3))
    min_rating = divParam(divIndex, cfgParameters.get("min_rating", 0))
    max_rating = divParam(divIndex, cfgParameters.get("max_rating", 0))
    boards = details['boards']

    #   "settings": {
    #     "rules": "chess",
    #     "time_class": "daily",
    #     "time_control": "1/259200",
    #     "initial_setup": "",
    #     "min_team_players": 10,
    #     "max_rating": 1600,
    #     "min_required_games": 10,
    #     "autostart": false
    # },
    settings = details['settings']
    match_rules = settings.get('rules', "chess")
    match_min_players = settings.get('min_team_players', 0)
    match_min_games = settings.get('min_required_games', 0)
    match_max_players = settings.get('max_team_players', 0)
    match_time_class = settings.get('time_class', "daily")
    match_time_control = settings.get('time_control', "none")
    match_min_rating = settings.get('min_rating', 0)
    match_max_rating = settings.get('max_rating', 0)
    errors = []
    if match_rules != rules:
        errors.append(f'Incorrect rules: {match_rules} should be {rules}')
    if match_min_players != min_players:
        boardsDesc = f"ONLY {boards} BOARDS!" if boards < min_players else f"{boards} boards"
        errors.append(f'Incorrect minimum number of players: {match_min_players} should be {min_players} ({boardsDesc})')
    if match_min_games != min_number_games:
        errors.append(f'Incorrect minimum number of games: {match_min_games} should be {min_number_games}')
    if match_max_players != max_players:
        errors.append(f'Incorrect maximum number of players: {match_max_players} should be {max_players}')
    if match_time_class != "daily":
        errors.append(f'Incorrect time class: {match_time_class} should be daily')
    if timeControl[match_time_control] != days_per_move:
        errors.append(f'Incorrect number of days per move: {timeControl[match_time_control]} should be {days_per_move}')
    if match_min_rating != min_rating:
        errors.append(f'Incorrect minimum rating: {match_min_rating} should be {min_rating}')
    if match_max_rating != max_rating:
        errors.append(f'Incorrect maximum rating: {match_max_rating} should be {max_rating}')
    return errors

def divToIndex(div):
    if div is int:
        divIndex = div
    else:
        for index, d in enumerate(divisions):
            if d == div:
                divIndex = index
    return divIndex

def stateJ(json):
    team = json['@id']
    return state(team)

# TODO rename to abbreviation()
def state(team):
    teamid = chess.teamId(team)
    return teamToAbbrev.get(teamid, teamToAbbrev.get(chess.teamUrl(teamid), teamid))

SCORES_FILE='scores.csv'

def usage():
    print("Usage: league.py [--scores] [--boards] [--table | --save | --compare] [--params] [--forfeits] ..")
    print("\t\t [--registration] [--running] [--completed] [--fpv] [--multiplayer] ..")
    print("\t\t [--since date] [--before date] [--verbose] [--refresh]\n")
    print("-scores:         report scores of matches")
    print("-boards:         details of boards in matches")
    print(f"-save:           write scores to '{SCORES_FILE}'")
    print(f"-read:           read scores from those stored in '{SCORES_FILE}'")
    print(f"-compare:        compare scores with those stored in '{SCORES_FILE}', report only changes")
    print("-add:            add a missing match or default interactively")
    print("-registration:   report matches in registration")
    print("-running:        report running matches")
    print("-completed:      report completed matches")
    print("-forfeits:       report forfeited matches due to insufficient players")
    print("-params:         report incorrect match parameters")
    print("-fpv:            report penalties due to FPV closures")
    print("-multiplayer:    report penalties due to multiplayers")
    print("-table:          report match results as a CSV table")
    print("-since YYYY-MM-DD: report matches started since specified date (default is read from config)")
    print("-start           same as -since")
    print("-before YYYY-MM-DD: report matches started before specified date")
    print("-end             same as -before")
    print("-verbose:        additional details and diagnosis")
    print("-refresh:        don't use cached information, load all data from chess.com")

def main():
    global verbose
    global start
    global before
    fpv = False
    multiplayer = False
    boards = False
    scores = False
    add = False
    readOnly = False
    save = False
    compare = False
    checkParams = False
    table = False
    registration = False
    running = False
    completed = False
    readStart = False
    readBefore = False
    reportForfeits = False
    startArg = None

    if len(sys.argv) < 2:
        usage()
        exit(1)

    chess.enableCache()
    for idx, arg in enumerate(sys.argv):
        if readStart:
            startArg = arg
            readStart = False
            continue
        if readBefore:
            before = arg
            readBefore = False
            continue
        if idx == 0:
            continue
        if arg.startswith("--"):
            arg = arg[1:]
        if arg == "-registration":
            registration = True
        elif arg == "-running" or arg == "--inprogress":
            running = True
        elif arg == "-completed":
            completed = True
        elif arg == "-fpv":
            fpv = True
        elif arg == "-multiplayer":
            multiplayer = True
        elif arg == "-forfeit" or arg == "-forfeits":
            reportForfeits = True
        elif arg == "-scores":
            scores = True
        elif arg == "-params":
            checkParams = True
        elif arg == "-add":
            add = True
        elif arg == "-read":
            readOnly = True
        elif arg == "-save":
            save = True
        elif arg == "-compare":
            compare = True
        elif arg == "-table":
            table = True
        elif arg == "-start" or arg == "-since":
            readStart = True
        elif arg == "-before" or arg == "-end":
            readBefore = True
        elif arg == "-verbose":
            verbose = True
        elif arg == "-boards":
            boards = True
        elif arg == "-refresh":
            chess.refresh()
        elif arg == "-help":
            usage()
            exit(0)
        else:
            print("Unsupported option:", arg)
            usage()
            exit(1)

    if not add and not readOnly and not registration and not running and not completed:
        print("Specify -read or one or more of -registration, -running, or -completed")
        usage()
        exit(1)

    createLeagueData()
    if startArg:
        start = startArg

    print(f"League: {homecfg['league']}: {homecfg['url']}")

    if not readOnly and not add:
        findTeamMatches(start, before, registration, running, completed, fpv, pattern=pattern)

    if fpv or multiplayer:
        if fpv:
            detectFPV()
        if multiplayer:
            detectMultiplayers();
        reportPenalties()

    if add:
        match = inputMatch()
        print(matchReport(match))
        saveMatch(match, SCORES_FILE)

    else:
        #  'scores' option is implied by any of the following.
        if readOnly or table or save or compare or checkParams:
            scores = True
        if scores:
            checkScores(boards, save, compare, table, SCORES_FILE, 
                        readOnly=readOnly, reportForfeits=reportForfeits, checkParameters=checkParams)
        elif not fpv and not multiplayer:
            reportMatches(boards, registration)


if __name__ == "__main__":
    main()

