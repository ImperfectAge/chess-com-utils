#!/usr/bin/env python3

import json
import urllib.request
import sys
from datetime import date
from datetime import datetime

import chess

def clubProfile(team):
    admins = [f"@{a}" for a in chess.admins(team)]
    print(f'Admins: {", ".join(admins)}')

    members = chess.members(team)
    stats = dict()
    for x in members:
        ustats = chess.stats(x)
        stats[x] = ustats
    chess.histogram(stats)

def countryProfile(team):
    members = chess.members(team)
    chess.countryHistogram(members)

histogram = False
byCountry = False
team = ''
chess.enableCache()
for arg in sys.argv[1:]:
    if arg == '-country':
        byCountry = True
    elif arg == '-refresh':
        chess.refresh()
    else:
        team = arg

if team == '':
    cfg = chess.readConfig()
    if 'home' in cfg:
        home = cfg['home']
        team = home['team']

chess.printClubDetails(team)
if (byCountry):
    countryProfile(team)
else:
    clubProfile(team)
