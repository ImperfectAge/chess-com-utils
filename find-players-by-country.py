#!/usr/bin/env python3

import json
import urllib.request
import sys
from datetime import date
from sys import stderr
from time import sleep
import re

import chess

teamMembers = set()

def findPlayers(team, country, locations) :

    locs = locations.split(',')
    regex = ""
    for loc in locs: 
    #    if loc.strip() != "":
        if regex != "":
            regex += '|'
        regex += ".*" + loc.strip() + ".*"
    pattern = re.compile(regex, re.IGNORECASE)

    teamMembers = chess.members(team)

    country_members = chess.country_players(country)
    if 'players' in country_members:
        for x in country_members['players']:
            if (x in teamMembers) :
                print(x, "is a member of", team, file=stderr)
            else :
                chess.checkUser(x, pattern)

team = ""
country = ""
locations = ""

chess.enableCache()
index = 1
if (len(sys.argv) > index):
    if (sys.argv[index] == "-refresh"):
        chess.refresh()
        index += 1

cfg = chess.readConfig()
if 'home' in cfg:
    home = cfg['home']
    team = home['team']
    country = home['country']
    locations = home['locations']
elif (len(sys.argv) >= 3):
    team = sys.argv[index]
    index += 1
    country = sys.argv[index]
    index += 1
    locations = sys.argv[index]

if team == "" or country == "" : #or locations == "":
    print(sys.argv[0], " will read config.txt with a [home] section containing  team, country and locations")
    print("Alternative usage:", sys.argv[0], "[team-name country locations]", file=stderr)
else:
    for c in country.split(',') :
        findPlayers(team, c.strip(), locations)

