#!/usr/bin/env python3

import sys
import chess

## S17 2019 Classical Open R4: The Golden Pheonix vs Outback
#url = "https://api.chess.com/pub/match/1051476"
# OR
#url = "1051476"

if (len(sys.argv) < 2):
    print("Usage:", sys.argv[0], " match-id [ team ]")
    exit(1)

snapshot = False
snaponly = False
chess.enableCache()

if (len(sys.argv) > 1):
    index = 1
    if sys.argv[index].startswith('--'):
        sys.argv[index] = sys.argv[index][1:]
    if sys.argv[index] == '-snapshot':
        snapshot = True
        index += 1
    elif sys.argv[index] == '-snaponly':
        snaponly = True
        index += 1
    if sys.argv[index] == '-refresh' :
        chess.refresh()
        index += 1
    matchid = sys.argv[index]
    if snaponly:
        chess.snapshot_match(matchid)
    else:
        chess.printMatch(matchid, boards=True, snapshot=snapshot)

