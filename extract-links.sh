#!/bin/bash

# Extract hrefs from a block of HTML.  
sed -n 's/.*href="\([^"]*\).*/\1/p' "$@"
echo

# For example, use it like this to find club names from a league
#curl -s https://www.chess.com/clubs/forum/view/wcl-2024-team-contacts | extract-links.sh  | grep /club/ | sed -e 's@https://www.chess.com/club/@@'


