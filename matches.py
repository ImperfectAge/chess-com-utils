#!/usr/bin/env python3

import json
import urllib.request
import sys
from datetime import date
from datetime import datetime
from sys import stderr
from time import sleep
import re

import chess

since = '2018-01-01'
before = None

def usage():
    print("Usage:", sys.argv[0], "[-refresh] [-verbose] [-since date] [-before date] [-finished | -inprogress | -registered] [team-name]")
    print("-since YYYY-MM-DD: report matches started since specified date")
    print("-before YYYY-MM-DD: report matches with start date before specified date")

chess.enableCache()
if len(sys.argv) > 0:
    match_states = []
    idx = 1
    verbose = False
    readSince = False
    readBefore = False
    team = chess.homeTeam()

    while idx < len(sys.argv):
        arg = sys.argv[idx]
        if arg.startswith('--'):
            arg = arg[1:]
        if readSince:
            since = arg
            readSince = False
        elif readBefore:
            before = arg
            readBefore = False
        elif (arg == '-v') or (arg == '-verbose') :
            verbose = True
        elif (arg == '-refresh') :
            chess.refresh()
        elif arg in ['-c', '-completed', '-done', '-f', '-finished'] :
            match_states.append( 'finished')
        elif arg in ['-i', '-inprogress', '-in_progress', '-running'] :
            match_states.append( 'in_progress')
        elif arg in ['-r', '-registered', '-registration', '-open', '-upcoming'] :
            match_states.append( 'registered')
        elif arg == "-start" or arg == "-since":
            readSince = True
        elif arg == "-before" or arg == "-end":
            readBefore = True
        elif (arg.startswith('-')):
            print('unrecognized:', arg)
            usage()
            exit(1)
        else:
            team = arg
        idx += 1

    if not match_states:
        match_states.append( 'registered')
    if before:
        print("Looking for matches starting after", since, 'and before', before, file=stderr)
    else:
        print("Looking for matches starting after", since, file=stderr)
    sinceDate = int(datetime.strptime(since, '%Y-%m-%d').timestamp())
    beforeDate = 0
    if before:
        beforeDate = int(datetime.strptime(before, '%Y-%m-%d').timestamp())
    #findTeamMatches(since, before, registration, running, completed, fpv)
    chess.clubMatches(team, match_states, verbose, start=sinceDate, before=beforeDate)

