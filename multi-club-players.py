#!/usr/bin/env python3

import sys
from datetime import datetime

import chess

verbose = False
csv = False
html = False

primaryTeams = {}       # map of username to list of team ids (string)
divteams = {}           # teams in each division
allTeams = []
playerMatches = {}      # map of username to list of matches they played
divisions = []          # list of divisions names from config
leaguetz = 'UTC'

class Team:
    def __init__(self, teamid, div):
        details = chess.clubDetails(teamid)
        self.name = details['name'] if details and 'name' in details else teamid
        self.id = teamid
        self.div = div
        self.matches = []

def findPlayersFromTeams():
    # Duplicate players are based on current team membership in this mode
    print("Looking for players belonging to multiple teams")
    for team in allTeams:
        team.players = chess.members(team.id)
        print("Team:", team.name, ':', len(team.players), 'members')

def findPlayersFromMatches():
    global playerMatches

    pattern = homecfg.get('pattern', "")
    start = '2020-07-01'
    if 'start' in homecfg:
        start = homecfg['start']
    startDate = int(datetime.strptime(start, '%Y-%m-%d').timestamp())
    beforeDate = 0
    if 'end' in homecfg:
        before = homecfg['end']
        beforeDate = int(datetime.strptime(before, '%Y-%m-%d').timestamp())
    print("Looking for matches starting after", start)
    # First we find the matches of interest for each team. These are the current season and matches against opponents in same division
    states = ["finished", "in_progress", "registered"]
    for team in allTeams:
        # Get finished matches, then in-progress, then ones in registration
        team.matches = chess.clubMatchList(
            team.id, states, start=startDate, before=beforeDate, opponents=divteams[team.div], pattern=pattern)
        # Matches is a list of tuples (matchid, json)
        team.matches.sort(key=lambda m: m[1].get('start_time',0) )
    playerMatches = findPlayersFromTheseMatches(allTeams)

def findPlayersFromTheseMatches(allTeams):
    # We extract the players from those matches
    playerMatches = {}      # map of username to list of matches they played
    for team in allTeams:
        team.players = set()
        for match, json in team.matches:
            players = chess.match_players(match, team.id)
            team.players |= players
            # Also remember all the matches for each player. TODO would be better to only do this for multi team players
            for player in players:
                if not player in playerMatches:
                    playerMatches[player] = []
                playerMatches[player].append(match)
        if verbose:
            print("Team: ", team.name, team.id, 'Div ' + team.div,
                  str(len(team.matches)) + ' matches', str(len(team.players)) + ' players', sep='\t')
    if verbose:
        print()
    return playerMatches

def reportMatch(name, url, date="", note=""):
    if html:
        print('      ', f"<a href='{url}'>{name}</a>", date, note, sep=',')
    elif csv:
        print(
            '      ', f"\"=HYPERLINK(\"\"{url}\"\",\"\"{name}\"\")\"", date, f"\"{note}\"", sep=',')
    else:
        print('      ', name, url, date, note)

def reportPenalty(homename, homescore, oppsname, oppscore, penalty_applied):
    if penalty_applied == 0:
        print("         No Adjustment:", homename, str(homescore) + ",", oppsname, oppscore)
    else:
        newhomescore  = homescore - penalty_applied
        newoppscore   = oppscore  + penalty_applied
        changedResult = homescore >= oppscore and (homescore - oppscore) <= penalty_applied*2 
        print("         Adjustment:", homename, str(homescore) + ",", oppsname, oppscore, "\t=>\t", \
                                    homename, str(newhomescore) + ",", oppsname, newoppscore, "CHANGED RESULT" if changedResult else "")

def report(name, url, note=""):
    if html:
        print('    ', f"<a href='{url}'>{name}</a>", note, sep=',')
    elif csv:
        print(
            '    ', f"\"=HYPERLINK(\"\"{url}\"\",\"\"{name}\"\")\"", f"\"{note}\"", sep=',')
    else:
        print('    ', name, url, note)

def findAllDupPlayers(allTeams, divisions, playerMatches):
    allplayers = set()
    # map player to list of teams (Team object) they played for
    dupPlayerTeams = {}

    for team in allTeams:
        dupPlayers = allplayers.intersection(team.players)
        team.dupPlayers = dupPlayers
        allplayers.update(team.players)

    # Go through the teams a second to complete the duplicate players (so each team has the dup players)
    for team in allTeams:
        if len(team.dupPlayers) != 0:
            for oteam in allTeams:
                if oteam == team:
                    continue
                intersection = oteam.players.intersection(team.dupPlayers)
                oteam.dupPlayers.update(intersection)

    # Go through the teams a third time to get the list of clubs each multiteam player played for
    for div in divisions:
        for team in allTeams:
            if team.div != div:
                continue
            for player in team.dupPlayers:
                if player not in dupPlayerTeams:
                    dupPlayerTeams[player] = list()
                dupPlayerTeams[player].append(team)

    # Some players have a primary team defined in the config.
    # Otherwise, find the earliest starting match for the player.  That will be their primary team.
    for player in dupPlayerTeams:
        if player not in primaryTeams:
            earliest = sys.maxsize
            earliestTeam = None
            for team in dupPlayerTeams[player]:
                for match, json in team.matches:
                    # make sure it's one of the player's matches
                    if match in playerMatches[player] and 'start_time' in json:
                        matchStart = json.get("start_time", 0)
                        if matchStart != 0 and matchStart < earliest:
                            if verbose:
                                print('@'+player, "earliest match now",
                                      json['name'], 'start=', chess.tzDate(matchStart, leaguetz))
                            earliest = matchStart
                            earliestTeam = team.id
            primaryTeams[player] = earliestTeam

    return dupPlayerTeams


def reportActiveMultiPlayers(byTeams, divisions, dupPlayerTeams):
    # Now start reporting..
    print("\nMulti-team players - by Division:")
    anyDup = False
    for div in divisions:
        print("\nDivision", div)
        for team in allTeams:
            if team.div != div:
                continue

            # Don't report players if this is thier primary team
            # Report the non-primary team matches in registration the player is signed up for
            dups = []
            if verbose:
                if team.dupPlayers:
                    print(team.name, end=': ')  
                    [print('@' + p, end=' ') for p in team.dupPlayers]
                    print()
                else:
                    print(f"{team.name} has no dup players.")

            for player in team.dupPlayers:
                if player not in primaryTeams or team.id != primaryTeams[player]:
                    dups.append(player)
            if verbose and team.matches:
                print(f"  matches: {[j['name'] for m, j in team.matches]}")

            if not byTeams:
                # If the team has no match in registration, just issue a warning and continue
                registration = False
                for match, json in team.matches:
                    # is the match in registration phase?
                    if 'state' in json and json['state'] == 'registered':
                        registration = True
                        break
                if not registration:
                    print(f"** {team.name} does not have a match in registration!")
                    continue

            for player in dups:
                found = False
                if verbose and player in playerMatches:
                    print(f"{player} was in matches: {playerMatches[player]}")
                # only report this player if they have a match in registration
                for match, json in team.matches:
                    # is the match in registration phase?
                    if 'state' in json and json['state'] == 'registered':
                        if verbose and player in playerMatches:
                            print(f"Checking {player} in {team.name} registration match: {json['name']} => {match in playerMatches[player]}")
                        if match in playerMatches[player]:
                            found = True
                            break
                if found:
                    anyDup = True
                    if csv or html:
                        report(team.name, chess.teamUrl(team.id), "@" + player)
                    else:
                        print(team.name + ":", "Please remove", "@" + player, "from this match")
                    reportMatch(json['name'], chess.matchUrl(json['@id']))
                    # report the player's first match for thier primary team
                    for team2 in dupPlayerTeams[player]:
                        if player not in primaryTeams or team2.id == primaryTeams[player]:
                            for match2, json2 in team2.matches:
                                if match2 in playerMatches[player]:
                                    matchStart = json2.get("start_time", 0)
                                    print("  @" + player, "first played for", team2.name, chess.matchUrl(json2['@id']), chess.tzDate(matchStart, leaguetz))
                                    break
                            break
                    print()

    print()
    if anyDup and 'ruletext' in homecfg:
        print(homecfg['ruletext'], "\n")

# Designed to be called from another module
def detectMatchMultiplayers(allTeams, divisions):
    playerMatches = findPlayersFromTheseMatches(allTeams)
    detectMultiplayerPenalties(allTeams, divisions, playerMatches)


def detectMultiplayerPenalties(allTeams, divisions, playerMatches):
    dupPlayerTeams = findAllDupPlayers(allTeams, divisions, playerMatches)
    for player in sorted(dupPlayerTeams):
        # Report the other team and team matches the player played for
        for team in dupPlayerTeams[player]:
            if player in primaryTeams and team.id == primaryTeams[player]:
                continue
            for match, json in team.matches:
                if match in playerMatches[player]:
                    # Calculate penalty to be applied to match if player is DQ'd
                    matchDetails = chess.match_details(match)
                    teams = matchDetails["teams"]
                    team1ID = chess.teamId(teams["team1"]["url"])
                    home = "team1" if team1ID == team.id else "team2"
                    teamj = teams[home]

                    homescore = teamj["score"]
                    penalty_applied = 0
                    for u in teamj["players"]:
                        if (u["username"].lower() == player):
                            if 'board' in u:
                                board = chess.match_board(u['board'])
                                score = board['board_scores'][player]
                                penalty_applied += score
                    # We need to store the calculated penalty with the match details for this team 'teamj'
                    # league.py has code to report these!
                    teamj['penalty'] = teamj.get('penalty', 0) + penalty_applied
                    if 'multiplayer' not in teamj:
                        teamj['multiplayer'] = []
                    teamj['multiplayer'].append(player)


def reportCalculatedPenalties():
    for team in allTeams:
        for match, json in team.matches:
            matchDetails = chess.match_details(match)
            teams = matchDetails["teams"]
            team1ID = chess.teamId(teams["team1"]["url"])
            home = "team1" if team1ID == team.id else "team2"
            opps = "team2" if team1ID == team.id else "team1"
            teamj = teams[home]
            # This code only looks at penalties from one team. Possible that both teams have multiplayers.  league.py does this better!
            if 'penalty' in teamj and teamj['penalty'] != 0:
                state = json.get('state', 'unknown')
                matchStart = json.get("start_time", 0)
                reportMatch(json['name'], chess.matchUrl(json['@id']), chess.tzDate(matchStart, leaguetz), f"({state})")
                # Calculate penalty to be applied to match if player is DQ'd
                homescore = teams[home]["score"]
                oppsscore = teams[opps]["score"]
                penalty_applied = teamj['penalty']
                for player in teamj.get('multiplayer', []):
                    print(f"{teams[home]['name']} player @{player} - Multiplayer")
                reportPenalty(teams[home]["name"], homescore, teams[opps]["name"], oppsscore, penalty_applied)


def findMultiPlayers(byTeams):
    dupPlayerTeams = findAllDupPlayers(allTeams, divisions, playerMatches)

    if verbose:
        print()
        print("Dup Player Teams: ")
        for p, teams in sorted(dupPlayerTeams.items()):
            print('  ', '@' + p, '-', len(teams), "teams", end=': ')
            for t in teams:
                print(t.name, end=', ')
            print()

    reportActiveMultiPlayers(byTeams, divisions, dupPlayerTeams)

    print("\nMulti-team players detail:\n")
    for player in sorted(dupPlayerTeams):
        # Report the primary team and matches the player played for
        print(f"@{player}")
        chess.reportDivisions(player, divteams)
        for team in dupPlayerTeams[player]:
            if player not in primaryTeams or team.id != primaryTeams[player]:
                continue
            report(team.name, chess.teamUrl(team.id), f"(@{player} Primary team)")
            for match, json in team.matches:
                if match in playerMatches[player]:
                    # is the match in registration phase?
                    registration = 'state' in json and json['state'] == 'registered'
                    matchStart = json.get("start_time", 0)
                    if registration:
                        reportMatch(json['name'], chess.matchUrl(
                            json['@id']), chess.tzDate(matchStart, leaguetz), '(Reg.)')
                    else:
                        reportMatch(json['name'], chess.matchUrl(
                            json['@id']), chess.tzDate(matchStart, leaguetz))

        # Report the other team and team matches the player played for
        for team in dupPlayerTeams[player]:
            if player in primaryTeams and team.id == primaryTeams[player]:
                continue
            report(team.name, chess.teamUrl(team.id), f"(@{player} Multi-team)")
            for match, json in team.matches:
                if match in playerMatches[player]:
                    # is the match in registration phase?
                    state = json.get('state', 'unknown')
                    matchStart = json.get("start_time", 0)
                    if state == 'registered':
                        reportMatch(json['name'], chess.matchUrl(json['@id']), 
                                    chess.tzDate(matchStart, leaguetz), 
                                    '(REGISTRATION TO BE REMOVED)')
                    else:
                        reportMatch(json['name'], chess.matchUrl(json['@id']), 
                                    chess.tzDate(matchStart, leaguetz), 
                                    '(' + state + ')')

        print()

def usage():
    print("Usage: multi-club-players.py [-teams] [-penalty] [-verbose] [-html | -csv] [-refresh]\n")
    print("-teams:   report multi-team members only, not by match signups")
    print("-penalty: analyze and report multiplayer penalties")
    print("-verbose: additional details and diagnosis")
    print("-html:    html format output instead of plain text")
    print("-csv:     csv format output instead of plain text")
    print("-refresh: don't use cached information, load all data from chess.com")

def identifyTeams():
    global homecfg
    global divisions
    global primaryTeams
    global divteams
    global allTeams
    global leaguetz

    cfg = chess.readConfig()
    if 'home' not in cfg:
        print("Multi-club-players.py: Could not find [home] section")
        sys.exit(3)
    homecfg = cfg['home']
    leaguetz = homecfg.get('timezone', 'UTC')

    if 'divisions' not in cfg:
        print("Multi-club-players.py: Could not find [divisions] section")
        sys.exit(5)
    divisions = cfg['divisions']

    if 'primary-teams' in cfg:
        for player in cfg['primary-teams']:
            primaryTeams[player] = cfg['primary-teams'][player].strip()
    if verbose:
        print('initial primary teams', primaryTeams)

    for div in divisions:
        divteams[div] = [x.strip() for x in divisions[div].split(',')]

    # Create the team objects, which includes the current membership of that club
    for div in divisions:
        teams = divteams[div]
        for teamid in teams:
            team = Team(teamid, div)
            allTeams.append(team)

def main():
    global verbose, html, csv
    byTeams = False
    penalty = False
    chess.enableCache()
    for idx, arg in enumerate(sys.argv):
        if idx == 0:
            continue
        if arg.startswith("--"):
            arg = arg[1:]
        elif arg == "-teams":
            byTeams = True
        elif arg == "-verbose":
            verbose = True
        elif arg == "-html":
            html = True
        elif arg == "-csv":
            csv = True
        elif arg == "-penalty" or arg == "-penalties":
            penalty = True
        elif arg == "-refresh":
            chess.refresh()
        elif arg == "-help":
            usage()
            exit(0)
        else:
            print("Unsupported option:", arg)
            usage()
            exit(1)

    identifyTeams()
    if byTeams:
        findPlayersFromTeams()
    else:
        findPlayersFromMatches()

    if penalty:
        detectMultiplayerPenalties(allTeams, divisions, playerMatches)
        reportCalculatedPenalties()
    else:
        findMultiPlayers(byTeams)

if __name__ == "__main__":
    main()

