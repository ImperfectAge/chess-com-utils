# Chess.com club management utilities

This is a collection of utilities for working with the publish data API from chess.com.
These are utilities I've found useful in managing chess.com teams and leagues.
Included utilities help with recruiting for teams, for team matches, detection of multi-players and player reporting.

See [Chess.com published data API](https://www.chess.com/news/view/published-data-api)

The largest file [chess.py](chess.py) is a collection of utility functions and is accessed by "import chess".
Each of the other files is a utility program that uses the functions in chess.py

## Required Packages
`dependencies.txt` contains a list of python packages required to be installed.

If you are using `pip`, the following will install the packages:

    pip3 install -r ~/proj/chess/requirements.txt 

## Club Management

*team-id* is the lowercase name, with spaces replaced by '-', of the chess.com team as seen in URLs, such as "the-golden-phoenix".  Sometimes this is called the 'slug'.  The club URL can be optionally given.


**matches.py** [-verbose] [-finished | -inprogress ] *team-id*

Find a list of team matches currently in registration for the named team.

If -finished or -inprogress is specified the report is for matches in the specified state.

If -verbose is specified a small report is also given for each match, similar to 'match.py' but without listing all the boards.


**match.py** [-snapshot | -snaponly] *match-id* *team-id*   

Generate a list of players signed up for a team match, along with rating and timeout-ratio.

This is useful for identifying those players with a high timeout-ratio in case admins wish
to exclude players based on timeout ratio.  It will show those with a timeout-ratio > 5%.
The rest is up to the admins ;)

* -snapshot option will also generate snapshot images (JPG) for each page of players.
* -snaponly option will only generate the snapshots, not the report.


**members.py** *team-id*   

Generate a list of members of a particular team, along with their join date.

This includes a report of rating, max rating (highest from any time control), and timeout ratio.

Usage:

members.py [-daily|-960|-blitz|-rapid|-bullet] [-clubs] [-country] [-league] [-joined] [-histogram] [-refresh] [-verbose] [team]


* -960, -blitz etc are used to report ratings for that style of chess
* -clubs will list each member's other clubs
* -joined will show the date each member joined the club
* -league requires the `[divisions]` section in the config file and reports which of the league's clubs each player is a member of
* -histogram reports how many players are in each rating band for the club
* -verbose provides all the information
* -refresh will load all information again from chess.com, and update the local cache


**not-signed-up.py**  [-csv] *team match-id*

Usage: not-signed-up.py  [-csv] [-verbose] [-any] [-team <team>] <match-id> [ <match-id> ...]

Generate a report of team members who are eligible for a specified match but not yet signed up.

This can also report on members who did not sign up (or did not play if there was max number of boards) for any or all of the specified matches.

The match-id is the final, numeric, section of the chess.com URL for a match. The full URL may be optionally given.

This will account for the criteria of the match, and will also exclude those players who are 
members of the opposing team, as well as those recorded in `config.txt` as do-not-disturb, and those with a high (> 50%) recent timeout ratio.

* -team is used to specify the team we are interested in, otherwise this is read from the config
* -csv option will generate report in CSV (comma-seperated variable) format
* -verbose will provide more info on the members
* -any option is used to report only members who didn't sign up for any of the specified matches

## User Report

**player.py**  *username*   

Generate a report on the specified username.  Note the inital '@' is optional. It will report basic stats, current timeout percentage,
club membership and list of clubs where the player is also an admin.  Includes a report of rating, max rating (highest from any time control).

When run in a directory with a config.txt file:

1. If the config file contains `[criteria]` the script will evaluate the players suitability per the described recruitment criteria
2. If the config file contains `[divisions]` section, the script will report on league team membership.  

See Config File and Leagues below


**rating.py**  *username*

This will generate a quick summary of the player's rating at daily chess, 960 and blitz, only.


## Club Report

**club.py** team-id
This will show a simple summary of numbers of members and average rating for the club, followed by histogram of number of members in each rating band (100 point range)


## Recruitment Tools  

These tools use a set of team recruitment criteria (such as as rating, number of games/teams, timeout-ratio etc) specified in `[criteria]` in the config file. See CONFIG FILE below.

**find-players-from-match.py** [*match-id*]+

Finds potential players based on those signed for the specified team match or matches. 

Each *match-id* is the number from the end of URL of a chess.com team match, or the whole API match url 

This search is based on criteria (such as as rating, number of games/teams, timeout-ratio etc) as specified in `[criteria]` in the config file.

The players will be be excluded from the generated list if they are already members of the 'target-team' per the config file.

See CONFIG FILE below for details on the criteria specification.


**find-players-from-teams-matches.py** *team-name*    

Finds potential players based on those who played team matches for the named team. 

*team-name* is the lowercase name (with spaces replaced by '-') of the chess.com team as seen in URLs, such as "the-golden-phoenix"

This search is based on criteria (such as as rating, number of games/teams, timeout-ratio etc) as specified in `[criteria]` in the config file.

The players will be be excluded from the generated list if they are already members of the 'target-team' per the config file.

See CONFIG FILE below for details on the criteria specification.


**find-players-by-country.py**  *team-name* *country* *search-string* *starts-with*   

Finds potential members for geographical based teams. 

Note due to recent changes to the Geographical players listing endpoint by chess.com,
this script may not work very well! This is because chess.com will only return a small number of players by country, 500, I think. And the same 500 each time :(

Uses the config as below.

* *team-name* is the lowercase name of the chess.com team as seen in URLs, such as "the-golden-phoenix"
* *country* is the [two-letter country code](https://www.chess.com/news/view/published-data-api#pubapi-endpoint-country)
* *search*-string is a pattern for matching memeber location information (such as 'oregon')
* *starts-with* is a string such as 'a' to find usernames beginning with a.  May be blank but you need to specify ""

**find-players-from-file.py**  *filename*

Given a simple text file with a list of chess.com usernames, one per line, will evaluate each player's suitability per the `[criteria]`
from the config file, and generate a list of recruitment candidates.  These candidates are grouped into sets of 30 players, so they can be invited
using the chess.com 'Invite Players' tool (one set of 30 per day).


## League Management

These scripts require the `[divisions]` section of the config.txt file. See LEAGUES below.  

NOTE. The league scripts find all matches between clubs in each division, between the dates specified by `start` and `end` in the config file.
All such matches between clubs in the same division are considered to be league matches - there's no exclusion mechanism at present for in-season friendly matches.

**league.py**

Usage: league.py [-fpv] [-registration] [-running] [-completed] [-registration] [-scores] [-boards] [-table | -save | -compare] [-since date] [-before date] [-verbose] [-refresh]

* -registration:   report matches in registration
* -running:        report running matches
* -completed:      report completed matches
* -scores:         report scores of matches
* -boards:         details of boards in matches
* -fpv:            report penalties due to FPV closures
* -multiplayer:    report penalties due to multiplayers
* -save:           write scores to 'scores.csv' 
* -compare:        compare scores with those stored in 'scores.csv', report only changes
* -table:          report match results as a CSV table
* -since YYYY-MM-DD: report matches started since specified date (default is read from config)
* -start           same as -since
* -before YYYY-MM-DD: report matches started before specified date
* -end             same as -before
* -verbose:        additional details and diagnosis
* -refresh:        don't use cached information, load all data from chess.com

League.py provides a number of options to report matches between league clubs. It can report open (in registration), and running or closed matches.  
This includes generating a table of past matches with results, and calaculating league scores/points based on matches, by checking the number of won games by each club in each match.  `-scores` option can be used to report wins where the majority of games are won by one of the teams.
With `-save` and `-compare` these results can be stored locally in a file and so report incremental (new) results only.

The `-fpv` and `-multiplayer` options look for Fair Play Violation players and Multi-team-players and apply penalties when calculating scores.

**multi-club-players.py**

From each match, it extracts the players and records the match where they first played for a particular team.
Finally it records and reports on any players who have played for multiple clubs in league matches.
For matches still in registration, it presents text that can be provided to the club admins about removing the multiplayers from those matches.

**fixtures.py**

This script can generate a complete set of fixtures for each division of a league.  It generates this in CSV format.
The formatting, team abbreviations etc, are designed for the USTCL (so USA states), but could be adapted for other leagues.

## Utilities

**snapshot.py**  *web-url* *filename* [*height*]   

Take a snapshot of the specified URL and save it as a JPG file.

Uses a web service and Headless Chrome to take the snapshot.
Sometimes chess.com flags this as unsupported browser, try retaking the snap at a different height

**shuffle.py**

Generate random pairings between sequences of names (player or club names, for example) specified on the command.  
Can be used for generating a tournament or league.

**states.py**  

This script is not intended to be run directly and just contains a mapping of US state name to the two letter abbreviation.
It's been extended to contain Canadian provinces as as a conveniance. 


# CONFIG FILE  

Several of the functions use information from a configuration file.  This file is always called config.txt and is only looked for in the current directory.
This file can contain the following sections.

`[home]`

The home section contains the following keys, each followed by an '=' sign

  * team: _team name_
  * country: _list of [two-letter country codes](https://www.chess.com/news/view/published-data-api#pubapi-endpoint-country)_
  * locations: _list of place names or partial names_
  * do_not_disturb: _list of team members to remove from match recruitment list_
  * start: _earliest start date to consider for league match detection_
  * end: _last start date to consider for league match detection_
  * ruletext: _text to use verbatum when reporting multiplayers_

Note: *country* and *locations* are used to search for recruitment candiates based on location information in the users profile.

For example, the `[home]` section might be as follows:

```
[home]
team = us-team-chess-league
do_not_disturb = 
start = 2022-07-01

ruletext = US Team Chess League Rule #6 states:
  Player eligibility: A player can only play for one team throughout a season regardless of division. 
  Once they play for a team, they have to stay with that team throughout the season.
```

`[criteria]`

Club recruitment tools such as *find-players-from-match* and *find-players-from-teams-matches.py* use `[criteria]` in the config file.
It's also used when reporting player details by the *player.py* script, if it's present in the current directory.  This can be useful for easily checking player suitability. 

This describes the criteria for recruitment.  
Criteria may be as follows
  * timeout_ratio: players daily chess timeout ratio
  * rating: players current daily chess rating 
  * clubs: number of clubs a player is member of
  * games: current number of active games member has
  * games_completed: number of games member has completed
  * games_won_percent: percentage of games member has won
  * member: number of days member has been on chess.com
  * glicko_rd: player's glicko RD score
  * last_logon: how many hours since the player's last login
  * exclude: list of users not to be included
  * exclude-teams: teams whose members are not be included

The values of each these of can be either 2 or 4 words seperated by spaces\
The first (and third) word is an operator, such as < > <= or <=.  The word after that is an integer value\

For example criteria.txt might be as follows:

```
[criteria]
target_team = my-team-that-im-recruiting-for
timeout_ratio = "< 15"
rating = "> 1100 < 1800"
clubs = "<= 50"
games = "> 5 < 60"
games_completed = "> 50"
games_won_percent = "< 70"
member = "> 30"
glicko_rd = "< 90"
last_logon = "< 48" 
#exclude = 
#exclude-teams = 
```

## LEAGUES

The config file contains an optional section [divisions].  The division section is a list of league divisions, one per line.  
Each line has the division name, and then a list of clubs in that division.  For example (where the divisions are called '1' & '2'):
This information is used for player reporting, but also for multi-player detection and for fixture generation.

    [divisions]
    1 = team-colorado, team-iowa, team-michigan, new-hampshire, new-jersey-chess-team, new-mexico-chess-players, north-carolina-usa-1, team-pennsylvania, south-carolina-usa, team-washington
    2 = alabama-chess, team-alberta-ii, northern-california-all-stars, team-indiana, team-minnesota, team-new-york, all-ohio, team-ontario, team-oregon, team-tennessee

# .ENV File

The `.env` file needs to be created before using these scripts. Copy the file called `env.example` to `.env`, and edit the file to include your own information.  This information will be sent to chess.com when making API calls, so they know who to contact if there are any problems.

    CHESSCOMUSER=@your-username-here
    EMAIL=your-email@address.com
    CACHEDIR=/some/path/to/store/users/cache/files/

If the values here are not legitimate, your API calls may fail or be blocked by chess.com

