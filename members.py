#!/usr/bin/env python3

import sys
from datetime import datetime
from datetime import date
from sys import stderr

import chess

verbose = False
histogram = False


def printLeagueInfo(details, fullName=False, joined=0, reportClubs=False, adminClubs=False, divisions={}):
    # printLeagueInfo(details, members[x], clubs)
    name = details.get("name", "")
    title = details.get("title", "")
    country = details.get("country", "")
    username = details["username"]
    location = details.get("location", "<>")

    if title != "" and not title.startswith(" "):
        title = " " + title
    if username[0] == '@':
        username = username[1:]
    print(str("@" + username + title).ljust(18), end="")
    if fullName:
        print(str('"' + name + '"').ljust(20), end='')
    if location != "":
        print(location.ljust(16), end="")
    if country != "":
        # Get the country name
        print(chess.countryName(country).ljust(12), end="")
    if joined > 0:
        print(" jnd", date.fromtimestamp(joined), end="")
    # Look for the player's division team(s)
    chess.reportDivisions(username, divisions, eol="")
    # print(f' (Member of {foundTeams} division teams)', end="")
    if reportClubs:
        chess.reportClubs(username) 
    if adminClubs:
        chess.reportAdminClubs(username)
    if not reportClubs and not adminClubs:
        print(flush=True)

chess.enableCache()
team = ""
rules = ""
country = False
location = False
joined = True
clubs = False
admins = False
league = False
name = False
for arg in sys.argv[1:]:
    if arg == "-v" or arg == "-verbose":
        verbose = True
    elif arg == "-hist" or arg == "-histogram":
        histogram = True
    elif arg == "-daily" or arg == "-chess":
        rules = "chess"
    elif arg == "-daily_960" or arg == "-960" or arg == "-chess960":
        rules = "chess960"
    elif arg == "-blitz":
        rules = "blitz"
    elif arg == "-rapid":
        rules = "rapid"
    elif arg == "-bullet":
        rules = "bullet"
    elif arg == "-name":
        name = True
    elif arg == "-country":
        country = True
    elif arg == "-location":
        location = True
    elif arg == "-clubs":
        clubs = True
    elif arg == "-admins":
        admins = True
    elif arg == "-league":
        league = True
    elif arg == "-refresh":
        chess.refresh()
    elif arg.startswith("-"):
        print(
            "Usage:",
            sys.argv[0],
            "[-daily|-960|-blitz|-rapid|-bullet] [-clubs] [-admins] [-country] [-location][-name] [-league] [-histogram] [-refresh] [-verbose] [team]",
            file=stderr,
        )
        exit(1)
    else:
        team = arg

divisions = {}           # teams in each division
if team == "":
    cfg = chess.readConfig()
    if "home" in cfg:
        home = cfg["home"]
        team = home["team"]

    if league:
        if 'divisions' not in cfg:
            print("-league requested, could not find [divisions] section")
            sys.exit(5)
        for div in cfg['divisions']:
            divisions[div] = [x.strip() for x in cfg['divisions'][div].split(',')]
        print("Divisions:", divisions)

if admins:
    print(f'Admins of {team}: {", ".join(chess.admins(team))}')

members = chess.membersWithJoinDate(team)
stats = dict()
# we want to report by rating (by default)
for x in members:
    ustats = chess.stats(x)
    stats[x] = ustats
if rules == "":
    rules = "chess"
print("Members of", team, ":", rules, "ratings")
if name:
    # Sort by username
    sorted = sorted(stats, key=lambda u: chess.user_details(u).get("username", ""), reverse=False)
elif country:
    # Sort by country
    sorted = sorted(stats, key=lambda u: chess.user_details(u).get("country", ""), reverse=False)
else:
    # Sort by rating
    sorted = sorted(stats, key=lambda u: chess.rating(stats[u], rules), reverse=True)
for x in sorted:
    details = chess.user_details(x)
    if not details:
        print("members.py: user", x, "not found", file=stderr)
    elif league and divisions:
        printLeagueInfo(details, joined=members[x] if joined else 0, reportClubs=clubs, adminClubs=admins, divisions=divisions)
    else:
        jnd = members[x] if joined else 0
        chess.printStats(details, stats[x], rules=rules, printCountry=country, printLoc=location, joined=jnd, clubs=clubs, adminClubs=admins)
if verbose or histogram:
    chess.histogram(stats)
