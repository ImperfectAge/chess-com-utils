#!/usr/bin/env python3

import json
import urllib.request
import sys
from datetime import date
from sys import stderr
from time import sleep
import re
import configparser
from operator import itemgetter

import chess

verbose = False
teamMembers = set()

def find_players(filename) :
    teamMembers = chess.members(team, fromMatches=False)
    if teamMembers:
        print("..excluding {} members of {}".format(len(teamMembers), team), file=stderr)
    else:
        print("..excluding members of {}".format(team), file=stderr)
    seen = set()

    # initialize seen from previously invited list
    try:
        with open("invited.txt", "r") as f:
            for line in f:
                # need to split each line .. and add all into 'seen'
                for name in line.strip().split():
                    seen.add(chess.stduser(name.strip()))
        print("..excluding {} previously-invited players".format(len(seen)), file=stderr)
    except:
        print("invited.txt not found, not excluding previously-invited names..", file=stderr)

    # we should only calculate the excluded team and users once!!
    exclude_users = chess.getExcludedPlayers(criteria['exclude'] if 'exclude' in criteria else "")
    exclude_clubs = chess.stringToSet(criteria['exclude-teams'] if 'exclude-teams' in criteria else "")

    eligible = []   # we will use this for sorting by rating
    if verbose: print ('..reading names from ', filename)
    count = 0
    with open(filename) as file_in:
        for line in file_in:
            # need to split each line .. and add all into 'seen'
            for u in line.strip().split():
                u = chess.stduser(u.strip())
                if u in seen:
                    if verbose: print("..already seen", u)
                    continue
                seen.add(u)
                count += 1
                if count % 200 == 0:
                    print(f"  ..checked {count} players", file=stderr)
                if chess.user_details(u) == None or chess.getClubs(u) == None:
                    continue
                if u in teamMembers if teamMembers else chess.isMember(u, team):
                    if verbose: print('..', u, "is a member of", team)
                    continue
                if u in exclude_users:
                    if verbose: print('..', u, "is an excluded user") 
                    continue
                if chess.isMemberOfAny(u, exclude_clubs):
                    if verbose: print('..', u, "is in an excluded club")
                    continue
                if chess.matchesStats(u, criteria, verbose=verbose, ratingRules="chess"):
                    stats = chess.stats(u)
                    rating = chess.rating(stats, "chess")
                    # put into list of 3-tuples for sorting
                    eligible.append((u, rating, stats))

    print()
    print("*** Recruitment candidates for", team, "***")
    print()

    # write the user info banded by 100-rating range
    chess.reportEligiblePlayers(eligible, verbose=verbose, sort=True)


chess.enableCache()
criteria = chess.configCriteria()
if criteria == {}:
    print("Failed to find config.txt")
    exit(1)

team = chess.homeTeam()

argv = sys.argv
argc = 1
arg = argv[argc]
if arg.startswith('--'): arg = arg[1:]
if arg == "-min":
    criteria['rating'] = "> " + argv[argc+1]
    argc += 2
elif arg == "-max":
    criteria['rating'] = "<= " + argv[argc+1]
    argc += 2
elif arg == "-verbose":
    verbose = True
    argc += 1
if len(argv) - argc < 1:
    print("Usage:", argv[0], "[ --verbose ] [ --min min-rating ] [ --max max-rating ] filename", file=stderr)
else:
    find_players(argv[argc])

