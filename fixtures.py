#!/usr/bin/env python3

import csv
import copy
from random import getstate
import sys

import chess


def calc_fixtures(orig_teams):
    allteams = copy.deepcopy(orig_teams)
    if len(allteams) % 2:
        allteams.append('Bye')
    n = len(allteams)
    fixtures = []
    for rd in range(1, n):
        matchs = []
        for i in range(n//2):
            match = (allteams[i], allteams[n - 1 - i])
            matchs.append(match)
        allteams.insert(1, allteams.pop())
        fixtures.insert(len(fixtures)//2, matchs)
    return fixtures

def team_url(team):
    return chess.teamUrl(team)

def stateName(team):
    return chess.stateName(team)

def make_hyperlink(url, display):
    # print('HYPERLINK(', url, ',', display, ')')
    return '=HYPERLINK("' + url + '","' + display + '")'

def create_matchups_table(stateTeams, teams, fixtures):
    matchups = []
    row = ['Team']
    nrounds = len(teams) + 1 if len(teams) % 2 else len(teams)
    for i in range(1, nrounds):
        row.append("R"+str(i))
    matchups.append(row)
    for team in teams:
        
        teamid = stateTeams[team]
        # print('team', team, 'teamid', teamid, 'url', team_url(teamid), 'name', stateName(team))
        row = [make_hyperlink(team_url(teamid), stateName(team))]
        for round in fixtures:
            for match in round:
                if (match[0] != team and match[1] != team):
                    continue        
                opp = match[0] if match[1] == team else match[1]
                # Any hyperlink in this cell should go the actual match when it exists
                row.append(opp)
        matchups.append(row)
    return matchups

# return a map of state abbreviation to the chess.com team id for the state
def readTeamInfo(fname):
    teams = dict()
    with open(fname, 'r', newline='') as f:
        r = csv.DictReader(f)
        for row in r:
            teams[row['State']] = chess.teamId(row['Link'])
    return teams

def readConfigDivisions():
    cfg = chess.readConfig()
    if 'home' not in cfg:
        print("Could not find [home] section")
        sys.exit(3)
    homecfg = cfg['home']
    if 'divisions' not in cfg:
        print("Could not find [divisions] section")
        sys.exit(5)
    divisions = cfg['divisions']
    divteams = {}
    for div in divisions:
        if div == 'waiting':
            continue
        divteams[div] = [x.strip() for x in divisions[div].split(',')]
    return divteams

# We need to get our team names as the state names
def getStateNameList(teamToState, teamids):
    teams = []
    for team in teamids:
        if team not in teamToState:
            print('Could not find', team, 'in state-clubs.csv')
            exit(2)
        teams.append(teamToState[team])
    # Need to sort by state name, not by abbreviation.  NM (New Mexico) and NC (North Carolina) are reversed otherwise
    return sorted(teams, key=lambda st: stateName(st))

chess.enableCache()
# read the CSV file state-clubs.csv.  This maps all state names to team URLs/ids
stateTeams = readTeamInfo('state-clubs.csv')
teamToState = dict(map(reversed, stateTeams.items()))
# Read the divisions from the config file.  The team names are chess.com team ids
divteams = readConfigDivisions()

for div in divteams:
    teams = getStateNameList(teamToState, divteams[div])
    print("Division", div, "teams: ", teams)
    fixtures = calc_fixtures(teams)
    # For generating the URL to the team, pass stateTeams dict in
    matchups = create_matchups_table(stateTeams, teams, fixtures)
    
    # print('Division', div, ':', matchups)
    divcsv = 'division' + div + '.csv'
    with open(divcsv, 'w', newline='') as f:
        w = csv.writer(f)
        w.writerows(matchups)
    print("Division", div, "matchups written to", divcsv)

