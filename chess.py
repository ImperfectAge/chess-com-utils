#!/usr/bin/env python3

import json
import re
import urllib.request
import http
import sys
from collections.abc import Mapping
from datetime import date
from datetime import datetime
from datetime import timedelta 
from operator import itemgetter
from sys import stderr
import zoneinfo
import time
import configparser
import shelve
import socket
import os
import html
import dotenv
from pathlib import Path
import states
import country
import itertools
import csv

import snapshot

dotenv.load_dotenv(dotenv.find_dotenv(".env"), verbose=True)

EMAIL = os.environ.get("EMAIL")
ACCOUNT = os.environ.get("CHESSCOMUSER")
CACHEDIR = os.environ.get("CACHEDIR")

if EMAIL is None:
    print("EMAIL not defined in environment, edit .env")
    sys.exit(1)
if ACCOUNT is None:
    print("CHESSCOMUSER not defined in environment, edit .env")
    sys.exit(1)
if CACHEDIR is None:
    print("CACHEDIR not defined in environment, edit .env")
    sys.exit(1)

HEADERS = {
    "User-Agent": "Python-urllib/3.11; chess.com user: " + ACCOUNT + "; email: " + EMAIL
}

# in-memory caches
USER_DETAILS = {}
USER_STATS = {}
USER_CLUBS = {}
MATCH_DETAILS = {}
MATCH_BOARDS = {}
TEAM_MATCHES = {}

# on-disk caches
USER_DETAILS_CACHE = {}
USER_STATS_CACHE = {}
USER_CLUBS_CACHE = {}
USER_GAMES_CACHE = {}
TEAMS_CACHE = {}

USE_CACHE = True

def enableCache():
    global USER_DETAILS_CACHE, USER_STATS_CACHE, USER_CLUBS_CACHE, USER_GAMES_CACHE, TEAMS_CACHE, USE_CACHE
    try:
        USER_DETAILS_CACHE = shelve.open(os.path.join(CACHEDIR, "details.shelve"), flag="c", protocol=None, writeback=False)
        USER_STATS_CACHE = shelve.open(os.path.join(CACHEDIR, "stats.shelve"), flag="c", protocol=None, writeback=False)
        USER_CLUBS_CACHE = shelve.open(os.path.join(CACHEDIR, "userclubs.shelve"), flag="c", protocol=None, writeback=False)
        USER_GAMES_CACHE = shelve.open(os.path.join(CACHEDIR, "games.shelve"), flag="c", protocol=None, writeback=False)
        TEAMS_CACHE = shelve.open(os.path.join(CACHEDIR, "teams.shelve"), flag="c", protocol=None, writeback=False)
        USE_CACHE = True
    except:
        USER_DETAILS_CACHE = {}
        USER_STATS_CACHE = {}
        USER_CLUBS_CACHE = {}
        USER_GAMES_CACHE = {}
        TEAMS_CACHE = {}
        USE_CACHE = False
        print('Shelve files are locked, not using disk cache...', file=stderr)

def closeCache():
    global USER_DETAILS_CACHE, USER_STATS_CACHE, USER_CLUBS_CACHE, USER_GAMES_CACHE, TEAMS_CACHE, USE_CACHE
    if USE_CACHE:
        USER_DETAILS_CACHE.close()
        USER_STATS_CACHE.close()
        USER_CLUBS_CACHE.close()
        USER_GAMES_CACHE.close()
        TEAMS_CACHE.close()
        USER_DETAILS_CACHE = {}
        USER_STATS_CACHE = {}
        USER_CLUBS_CACHE = {}
        USER_GAMES_CACHE = {}
        TEAMS_CACHE = {}
        USE_CACHE = False


def refresh():
    global USE_CACHE
    USE_CACHE = False

def isRefreshed():
    return not USE_CACHE

def teamId(teamUrl):
    # drop off any url, we only want the team id
    return str(teamUrl).rpartition("/")[-1]

def teamUrl(team):
    return "https://www.chess.com/club/" + teamId(team)

def stduser(u):
    u = str(u).rpartition("/")[-1]
    if u[0] == '@':
        u = u[1:]
    return u.lower()

def playerUrl(username):
    return "https://api.chess.com/pub/player/" + stduser(username)

def readConfig(fname="config.txt"):
    cfg = configparser.ConfigParser()
    # preserve case
    cfg.optionxform = str
    cfg.read(fname)
    return cfg

def printConfig(cfg):
    if cfg.defaults():
        print(f"[DEFAULTS]\n{cfg.defaults()}")
    for section in cfg.sections():
        print(f"[{section}]")
        printConfigSection(cfg[section])

def printConfigSection(cfgSec):
    for opt in cfgSec:
        print(f"\t{opt}={cfgSec[opt]}")

def homeTeam():
    cfg = readConfig()
    if 'home' in cfg and 'team' in cfg['home']:
        return cfg['home']['team']
    if "criteria" in cfg and 'target_team' in cfg['criteria']:
        return cfg['criteria']['target_team']
    return None

def configCriteria():
    cfg = readConfig()
    if "criteria" in cfg:
        return cfg["criteria"]
    return {}

# request: generic URL request returned as json
def request(url):
    myrequest = urllib.request.Request(url, None, HEADERS)
    try:
        with urllib.request.urlopen(myrequest, timeout=10) as response:
            response_bytes = response.read()
            response_json = json.loads(response_bytes.decode("utf8"))
            return response_json
        return None
    except urllib.error.URLError as e:
        print("URL error getting ", url, ":", e, file=stderr)
        return None

def tzDateTime(ts, tz):
    if ts == 0:
        return '<No start date>'
    tzi = zoneinfo.ZoneInfo(tz)
    dt = datetime.fromtimestamp(ts, tz=tzi)
    dt = dt.replace(microsecond=0)
    return dt.isoformat() + " " + dt.tzname()

def tzDate(ts, tz):
    if ts == 0:
        return '<No start date>'
    return datetime.fromtimestamp(ts, tz=zoneinfo.ZoneInfo(tz)).date()

# User details
def user_details(username, refresh=False):
    username = stduser(username)
    if not refresh and username in USER_DETAILS:
        return USER_DETAILS[username]
    if not refresh and USE_CACHE and username in USER_DETAILS_CACHE:
        try:
            details = USER_DETAILS_CACHE[username]
            USER_DETAILS[username] = details
            return details
        except Exception as e:
            print("Exception reading cache - ", str(e))
    sleep_count = 0
    while sleep_count < 10:
        sleep_count+=1
        url = playerUrl(username)
        my_request = urllib.request.Request(url, None, HEADERS)
        try:
            with urllib.request.urlopen(my_request, timeout=10) as response:
                response_bytes = response.read()
                details = json.loads(response_bytes.decode("utf8"))
                USER_DETAILS[username] = details
                USER_DETAILS_CACHE[username] = details
                return details
            return None
        except urllib.error.HTTPError as e:
            if e.code == 429:
                print("Sleeping due to HTTP too many requests getting details for ", username, ":", e, file=stderr)
                time.sleep(2)
                continue
            print("HTTP error getting details for ", username, ":", e, file=stderr)
            return None
        except urllib.error.URLError as e:
            if str(e) == "HTTP Error 429: Too Many Requests" or str(e) == "<urlopen error [Errno -3] Temporary failure in name resolution>":
                print( "Sleeping due to URL error getting details for ", username, ":", e, file=stderr)
                time.sleep(2)
                continue
            print("URL error getting details for ", username, ":", e, file=stderr)
            return None
        except http.client.RemoteDisconnected as e:
            if str(e) == "Remote end closed connection without response":
                print("Sleeping due to http.client.RemoteDisconnected getting details for ",
                      username, ":", e, file=stderr)
                time.sleep(5)
                continue
            print("HTTP error getting details for ", username, ":", e, file=stderr)
            return []
        except socket.timeout as e:
            print( "Sleeping due to socket timeout getting details for ", username, ":", e, file=stderr)
            time.sleep(10)
            continue
    return None


def stats(username, refresh=False):
    username = stduser(username)
    if not refresh and username in USER_STATS:
        return USER_STATS[username]
    if not refresh and USE_CACHE and username in USER_STATS_CACHE:
        try:
            player_stats = USER_STATS_CACHE[username]
            USER_STATS[username] = player_stats
            return player_stats
        except Exception as e:
            print("stats cache error - player", username, e, " - will overwrite",file=stderr)
    sleep_count = 0
    while sleep_count < 10:
        sleep_count+=1
        url = "https://api.chess.com/pub/player/" + username + "/stats"
        myrequest = urllib.request.Request(url, None, HEADERS)
        try:
            with urllib.request.urlopen(myrequest, timeout=10) as response:
                response_bytes = response.read()
                player_stats = json.loads(response_bytes.decode("utf8"))
                USER_STATS[username] = player_stats
                USER_STATS_CACHE[username] = player_stats
                return player_stats
        except urllib.error.HTTPError as e:
            if e.code == 429:
                print("Sleeping due to HTTP too many requests getting stats for ",
                      username, ":", e, file=stderr)
                time.sleep(2)
                continue
            print("HTTP error getting stats for ",
                  username, ":", e, file=stderr)
            return None
        except urllib.error.URLError as e:
            if str(e) == "HTTP Error 429: Too Many Requests" or str(e) == "<urlopen error [Errno -3] Temporary failure in name resolution>":
                print("Sleeping due to URL error getting stats for ", username, ":", e, file=stderr)
                time.sleep(2)
                continue
            print("URL error getting stats for ", username, ":", e, file=stderr)
            return None
        except http.client.RemoteDisconnected as e:
            if str(e) == "Remote end closed connection without response":
                print("Sleeping due to http.client.RemoteDisconnected getting stats for ",
                      username, ":", e, file=stderr)
                time.sleep(5)
                continue
            print("HTTP error getting stats for ", username, ":", e, file=stderr)
            return []
        except socket.timeout as e:
            print("Sleeping due to socket timeout getting stats for ", username, ":", e, file=stderr)
            time.sleep(10)
            continue
    return None

def playerStatus(player):
    details = user_details(player, refresh=False)
    if not details:
        return "unknown"
    return details.get("status", "")

# Returns a list of clubs including name, joined date, url etc
def getClubs(username, refresh=False):
    username = stduser(username)
    if not refresh and username in USER_CLUBS:
        return USER_CLUBS[username]
    if not refresh and USE_CACHE and username in USER_CLUBS_CACHE:
        try:
            clubs = USER_CLUBS_CACHE[username]
            USER_CLUBS[username] = clubs
            return clubs
        except Exception as e:
            print("Exception reading cache - ", str(e))
    url = "https://api.chess.com/pub/player/" + username + "/clubs"
    myrequest = urllib.request.Request(url, None, HEADERS)
    sleepCount = 0
    while sleepCount < 10:
        sleepCount += 1
        try:
            with urllib.request.urlopen(myrequest, timeout=10) as response:
                response_bytes = response.read()
                clubs = json.loads(response_bytes.decode("utf8"))
                if "clubs" in clubs:
                    c = sorted(clubs["clubs"], key=lambda x: x["name"])
                    USER_CLUBS[username] = c
                    USER_CLUBS_CACHE[username] = c
                    return c
                return []
        except urllib.error.HTTPError as e:
            if e.code == 429:
                print("Sleeping due to HTTP too many requests getting clubs for ",
                      username, ":", e, file=stderr)
                time.sleep(2)
                continue
            print("HTTP error getting clubs for ", username, ":", e, file=stderr)
            return None
        except urllib.error.URLError as e:
            if str(e) == "HTTP Error 429: Too Many Requests" or str(e) == "<urlopen error [Errno -3] Temporary failure in name resolution>" :
                print("Sleeping due to URL error getting clubs for ", username, ":", e, file=stderr)
                time.sleep(2)
                continue
            print("URL error getting clubs for ", username, ":", e, file=stderr)
            return None
        except http.client.RemoteDisconnected as e:
            if str(e) == "Remote end closed connection without response":
                print("Sleeping due to http.client.RemoteDisconnected getting clubs for ",
                      username, ":", e, file=stderr)
                time.sleep(5)
                continue
            print("HTTP error getting clubs for ", username, ":", e, file=stderr)
            return []
        except socket.timeout as e:
            print("Sleeping due to socket timeout getting clubs for ", username, ":", e, file=stderr)
            time.sleep(10)
            continue
    return None

def getAdminClubs(username):
    clubs = getClubs(username)
    for club in clubs:
        if not clubDetails(club["url"]):
            # stale list of clubs?
            clubs = getClubs(username, refresh=True)
            break
    adminClubs = []
    for club in clubs:
        if isAdmin(username, club["url"]):
            adminClubs.append(teamId(club["url"]))
    return sorted(adminClubs)

def isMember(player, club):
    clubs = getClubs(player)
    for c in clubs:
        if club == teamId(c['url']) :
            return True    
    # This is a check for the club name rather than club ID; I don't think it's used like that
    # fold = club.casefold()
    # for c in clubs:
    #     if fold == c["name"].casefold():
    #         return True
    return False

def isMemberOfAny(player, clubs):
    for c in clubs:
        if isMember(player, c):
            return True
    return False

def joinedDate(player, club):
    clubs = getClubs(player)
    for c in clubs:
        if club.casefold() == c["name"].casefold() or club == teamId(c['url']) :
            return c["joined"]
    return 0

def isAdmin(player, club):    
    url = playerUrl(player)
    clubdetails = clubDetails(club)
    return clubdetails and url in clubdetails["admin"]

def admins(club):
    clubdetails = clubDetails(club)
    if clubdetails:
        adminUsers = clubdetails.get("admin", [])
        return set(stduser(u) for u in adminUsers)
    return set()

def reportAdminClubs(username):
    adminClubs = getAdminClubs(username)
    if adminClubs:
        print("\tAdmin in", len(adminClubs), "clubs", end=": ")
        print(*[html.unescape(club) for club in adminClubs], sep=', ')

def num_clubs(u):
    return len(getClubs(u))

def getCurrentGames(username):
    username = stduser(username)
    url = "https://api.chess.com/pub/player/" + username + "/games"
    myrequest = urllib.request.Request(url, None, HEADERS)
    sleepCount = 0
    while sleepCount < 10:
        sleepCount+=1
        try:
            with urllib.request.urlopen(myrequest, timeout=10) as response:
                response_bytes = response.read()
                games = json.loads(response_bytes.decode("utf8"))
                if "games" in games:
                    return games["games"]
                return games
        except urllib.error.HTTPError as e:
            if e.code == 429:
                print("Sleeping due to HTTP too many requests getting games for ",
                      username, ":", e, file=stderr)
                time.sleep(2)
                continue
            print("HTTP error getting games for ",
                  username, ":", e, file=stderr)
            return []
        except urllib.error.URLError as e:
            if str(e) == "HTTP Error 429: Too Many Requests" or str(e) == "<urlopen error [Errno -3] Temporary failure in name resolution>" :
                print("Sleeping due to URL error getting games for ", username, ":", e, file=stderr)
                time.sleep(2)
                continue
            print("URL error getting games for ", username, ":", e, file=stderr)
            return []
        except http.client.RemoteDisconnected as e:
            if str(e) == "Remote end closed connection without response":
                print("Sleeping due to http.client.RemoteDisconnected getting games for ",
                      username, ":", e, file=stderr)
                time.sleep(5)
                continue
            print("HTTP error getting games for ", username, ":", e, file=stderr)
            return []
        except socket.timeout as e:
            print("Sleeping due to socket timeout getting games for ", username, ":", e, file=stderr)
            time.sleep(10)
            continue
    return []

def num_games(u, refresh=False):
    u = stduser(u)
    # return len(getCurrentGames(u))
    if not refresh and USE_CACHE and u in USER_GAMES_CACHE:
        try:
            return USER_GAMES_CACHE[u]
        except Exception as e:
            print("Exception reading cache - ", str(e))
    else:
        games = len(getCurrentGames(u))
        USER_GAMES_CACHE[u] = games
        return games

def members(team, fromMatches=True):
    team = teamId(team)
    url = "https://api.chess.com/pub/club/" + team + "/members"
    myrequest = urllib.request.Request(url, None, HEADERS)
    teamMembers = set()
    failed = None
    try:
        with urllib.request.urlopen(myrequest, timeout=15) as response:
            members_bytes = response.read()
            member_info = json.loads(members_bytes.decode("utf8"))
        for x in member_info["weekly"]:
            teamMembers.add(x["username"].lower())
        for x in member_info["monthly"]:
            teamMembers.add(x["username"].lower())
        for x in member_info["all_time"]:
            teamMembers.add(x["username"].lower())
    except urllib.error.HTTPError as e:
        failed = e
    except:
        failed = True
    # Get members from teams matches
    if failed and fromMatches:
        print(f"..{team} is a private club; searching from matches..", file=stderr)
        # Start date 2 years ago
        HOW_OLD_MATCHES = 2*365
        startDate = datetime.now() - timedelta(days=HOW_OLD_MATCHES)
        players = clubMatchPlayers(team, ["in_progress", "finished", "registered"], allTeams=False, start=startDate.timestamp())
        for u in players:
            # Make sure the player is still a member and not closed or closed FPV
            if isMember(u, team) and "closed" not in playerStatus(u):
                teamMembers.add(u)
    if failed and not teamMembers:    
        print("Cannot find club members", team, "at", url, ":", failed, file=stderr)

    return teamMembers


def membersWithJoinDate(team, fromMatches=True):
    team = teamId(team)
    url = "https://api.chess.com/pub/club/" + team + "/members"
    myrequest = urllib.request.Request(url, None, HEADERS)
    teamMembers = dict()
    failed = None
    try:
        with urllib.request.urlopen(myrequest, timeout=15) as response:
            members_bytes = response.read()
            member_info = json.loads(members_bytes.decode("utf8"))
        # store member join date for club
        for x in member_info["weekly"]:
            u = x["username"].lower()
            j = x["joined"]
            teamMembers[u] = j
        for x in member_info["monthly"]:
            u = x["username"].lower()
            j = x["joined"]
            teamMembers[u] = j
        for x in member_info["all_time"]:
            u = x["username"].lower()
            j = x["joined"]
            teamMembers[u] = j
    except urllib.error.HTTPError as e:
        failed = e
    except:
        failed = True
    # Get members from teams matches
    if failed and fromMatches:
        print(f"..{team} is a private club; searching from matches..", file=stderr)
        # Start date 2 years ago
        HOW_OLD_MATCHES = 2*365
        startDate = datetime.now() - timedelta(days=HOW_OLD_MATCHES)
        players = clubMatchPlayers(team, ["in_progress", "finished", "registered"], allTeams=False, start=startDate.timestamp())
        # We get the member's job date for the club in this case from the player info
        for u in players:
            # Make sure the player is still a member and not closed or closed FPV
            if isMember(u, team) and "closed" not in playerStatus(u):
                teamMembers[u] = joinedDate(u, team)

    if failed and not teamMembers:    
        print("Cannot find club members", team, "at", url, ":", failed, file=stderr)
    return teamMembers


def clubDetails(team, refresh=False):
    team = teamId(team)
    if not refresh and USE_CACHE and team in TEAMS_CACHE:
        try:
            return TEAMS_CACHE[team]
        except Exception as e:
            print("Exception reading cache - ", str(e))
    else:
        url = "https://api.chess.com/pub/club/" + team
        myrequest = urllib.request.Request(url, None, HEADERS)
        try:
            with urllib.request.urlopen(myrequest, timeout=10) as response:
                resp_bytes = response.read()
                x = json.loads(resp_bytes.decode("utf8"))
                TEAMS_CACHE[team] = x
                return x
        except urllib.error.HTTPError as e:
            print("Cannot find club", url, e, file=stderr)
        except urllib.error.URLError as e:
            print("Cannot find club", url, e, file=stderr)


def printClubDetails(team):
    x = clubDetails(team)
    if x:
        print(
            x["name"],
            x["url"],
            x["members_count"],
            "members",
            "Av. Rating:",
            x["average_daily_rating"],
        )
    else:
        print(f'{team} - lookup failed')


# If allTeams is True, return players from this team and their opponents
def clubMatchPlayers(team, match_states, allTeams=False, start=0, before=0) :
    matches = clubMatchesJson(team, match_states)
    all_match_players = set()
    matchTeam = "" if allTeams else team
    for x in matches:
        url = x["@id"]
        matchStart = x.get("start_time", 0)
        if matchStart == 0 or start == 0 or matchStart >= start:
            if matchStart == 0 or before == 0 or matchStart < before:
                mplayers = match_players(url, matchTeam)
                # add mplayers to set of all match players
                all_match_players |= mplayers
    return all_match_players

# clubMatchList: Return a list of tuples (matchID, json) where the json is
# that described here https://www.chess.com/news/view/published-data-api#pubapi-endpoint-club-matches
# If start is specified only matches that start on or after that date are specified (time is timestamp, secs since 1970)
# If opponents are specified, only matches against one of those opponents are included (club id is like team-oregon or the-golden-phoenix)
# Option Pattern is used to filter match names and include those including the named substring 
def clubMatchList(team, match_states, start=0, before=0, opponents=[], pattern=""):
    matches = clubMatchesJson(team, match_states)
    matchIds = list()
    regexp = re.compile('.*' + pattern, re.IGNORECASE)
    pattern = pattern.lower()
    for x in matches:
        opponent = teamId(x["opponent"])
        matchStart = x.get("start_time", 0)
        if matchStart == 0 or start == 0 or matchStart >= start:
            if matchStart == 0 or before == 0 or matchStart < before:
                if not opponents or opponent in opponents:
                    name = x["name"].lower()
                    if not pattern or pattern in name or regexp.match(name):
                        mid = matchId(x["@id"])
                        matchIds.append((mid, x))

    return matchIds

def stringToSet(names):
    return set([name.strip().lower() for name in re.split("\s|,", names)]) 

def stringToSetOfPlayers(names):
    return set([stduser(name.strip()) for name in re.split("\s|,", names)]) 

# Return a set of player names based on the list of names in 'exclude' and the list of teams in 'exclude_teams'.
# Each of the parameters is a string, with names separated by commas. Player names are lowercased.
def getExcludedPlayers(exclude):
    exclude_users = stringToSetOfPlayers(exclude)
    print("..excluding {} players from config.txt".format(len(exclude_users)), file=stderr)
    return exclude_users

def clubMatchesAll(team):
    global TEAM_MATCHES
    team = teamId(team)
    if team in TEAM_MATCHES:
        return TEAM_MATCHES[team]
    url = f"https://api.chess.com/pub/club/{team}/matches"
    allmatches = {}
    myrequest = urllib.request.Request(url, None, HEADERS)
    try:
        with urllib.request.urlopen(myrequest, timeout=30) as response:
            resp_bytes = response.read()
            allmatches = json.loads(resp_bytes.decode("utf8"))
            for state, matchList in allmatches.items():
                if state == 'comment':
                    continue
                matchList.sort(key=lambda m: m.get("start_time", 0), reverse=False)
                for match in matchList:
                    match["state"] = state
    except urllib.error.HTTPError as e:
        print("Cannot find club", team, e, file=stderr)
    TEAM_MATCHES[team] = allmatches
    return allmatches

def clubMatchesJson(team, match_states):
    if isinstance(match_states, str):
        return clubMatchesAll(team).get(match_states, [])
    else:
        result = []
        for state in match_states:
            result += clubMatchesAll(team).get(state, [])
        return result

def clubMatches(team, match_states, verbose, start=0, before=0):
    matches = clubMatchesJson(team, match_states)
    for match in matches:
        matchStart = match.get("start_time", 0)
        if matchStart == 0 or start == 0 or matchStart >= start:
            if matchStart == 0 or before == 0 or matchStart < before:
                reportClubMatch(match, team, verbose)

# reportClubMatch uses the json retreived from club matches API aka clubMatchesJson(team, match_states):
# See also printMatch which reports a match retrieved from the matches API match_details(match) 
def reportClubMatch(matchJson, team, verbose, tz='UTC', warnings=True):
    opponent = str(matchJson["opponent"]).rsplit("/", 1)[1]
    starttext = "Starts" if (matchJson["state"] == "registered") else "Started"
    start = matchJson.get("start_time", 0)
    starttime = "?" if start == 0 else tzDateTime(start, tz)
    result = team + ' ' + matchJson["result"] if "result" in matchJson else ""
    details = match_details(matchJson['@id'])
    settings = details.get("settings", matchJson.get("settings", {}))
    min_rating = settings.get("min_rating", 0)
    max_rating = settings.get("max_rating", 9999)
    rating = describeRating(min_rating, max_rating)
    min_players = settings.get("min_team_players", 0)
    max_players = settings.get("max_team_players", 9999)
    boards = details.get('boards', matchJson.get('boards', 0))

    teams = details["teams"]
    team1 = teams["team1"]["name"]
    team2 = teams["team2"]["name"]
    players1 = teams["team1"]["players"]
    players2 = teams["team2"]["players"]
    home = team1 if teamId(teams["team1"]["url"]) == team else team2

    if boards < min_players:
        # TODO do we have "score" : 0  ?
        #   "score":0,"result":"draw"
        #   So test for that combination and it's certain that it's a defaulted match, not a match that ended in a draw.
        # See https://www.chess.com/clubs/forum/view/changed-match-policy-results-in-incorrect-api-end-point-https-api-chess-com-pub-match-id?page=2&newCommentCount=4#comment-100436993
        # Check for failure to min reach number of players
        if matchJson["state"] == "finished" and matchJson["result"] == "draw":
            if verbose:
                print(f"** {matchJson['name']} failed to meet minimum number of players! " )
            result = matchJson["state"] = "aborted due to min players"
            starttext = "Start date"
            if len(players1) < min_players:
                result = team1 + ' failed to reach min number of players'
                if team1 == home: result = "WARNING: " + result.upper()
            if len(players2) < min_players:
                result = team2 + ' failed to reach min number of players'
                if team2 == home: result = "WARNING: " + result.upper()
            if len(players1) < min_players and len(players2) < min_players:
                result = 'Both teams failed to reach min number of players'

    # if still in registration check the status. Who is ahead?
    match matchJson["state"]:
      case "registered":
        if team1 == home and len(players1) < min_players:
            result = f"WARNING:{team1} has not enough players (min {min_players}, {min_players-len(players1)} short)".upper()
        elif team2 == home and len(players2) < min_players:
            result = f"WARNING:{team2} has not enough players (min {min_players}, {min_players-len(players2)} short)".upper()
        elif len(players1) < min_players and len(players2) < min_players:
            result = f"BOTH TEAMS ARE SHORT PLAYERS (min {min_players})"
        elif len(players1) < min_players:
            result = f"{team1} has not enough players (min {min_players}, {min_players-len(players1)} short)"
            if team1 == home: result = "WARNING: " + result.upper()
        elif len(players2) < min_players:
            result = f"{team2} has not enough players (min {min_players}, {min_players-len(players2)} short)"
            if team2 == home: result = "WARNING: " + result.upper()
        else:
            # matches in registration have ratings
            if len(players1) > 0:
                players1 = sorted(players1, key=lambda x: x.get("rating", 0), reverse=True)
            if len(players2) > 0:
                players2 = sorted(players2, key=lambda x: x.get("rating", 0), reverse=True)
            strength = 0
            adv1 = 0
            adv2 = 0
            for board in range(min(len(players1), len(players2))):
                u1 = players1[board]
                u2 = players2[board]
                rating1 = u1.get("rating", 0)
                rating2 = u2.get("rating", 0)
                if board < max_players:
                    strength += rating1 - rating2
                    if rating1 > rating2:
                        adv1 += 1
                    if rating2 > rating1:
                        adv2 += 1
            if adv1 > adv2:
                result = f"{team1} advantage: {adv1} vs {adv2} boards"
                if warnings and team2 == home: result = "WARNING: " + result.upper()
            elif adv2 > adv1:
                result = f"{team2} advantage: {adv2} vs {adv1} boards"
                if warnings and team1 == home: result = "WARNING: " + result.upper()
            else:
                result = f"Even match: {adv2} vs {adv1} boards"
                if warnings: result = result.upper()
            if warnings:
                result += f", strength {strength if team1 == home else -strength}"

      case "in_progress":
            score1 = teams["team1"]["score"]
            score2 = teams["team2"]["score"]
            result += f"Running - {score1}:{score2}"

    print(f'"{matchJson["name"].strip()}"', matchUrl(details), opponent, rating, boards, "boards", starttext, starttime, result)

    if verbose:
        url = matchJson["@id"]
        printMatch(url, boards=False)
        print()


def matchId(matchurl):
    if type(matchurl) is dict:
        if 'url' in matchurl:
            matchurl = matchurl['url']
    # drop off any url, we only want the numeric match id
    matchurl = str(matchurl)
    if matchurl.endswith("/games"):
        matchurl = matchurl.removesuffix("/games")
    return str(matchurl).rpartition("/")[-1]

def matchUrl(json):
    matchid = json
    if isinstance(json, Mapping):
        if 'url' in json:
            # return this URL directly.  This deals with daily matches too
            return json['url']
        elif '@id' in json:
            matchid = json['@id']
        else:
            print("matchUrl cannot be found in json..")
            print(json)
    # now put the daily match url at the front (this assumes it's a daily match!)
    url = "https://www.chess.com/club/matches/" + matchId(matchid)
    return url


def match_details(matchid):
    global MATCH_DETAILS
    shortId = matchId(matchid)
    if shortId in MATCH_DETAILS and MATCH_DETAILS[shortId]:
        return MATCH_DETAILS[shortId]
    # We might have been given the URL of a live match, not always daily
    if matchid.startswith("https://api.chess.com/pub/"):
        url = matchid
    else:
        # put the api url at the front of the short id (assumes daily chess match)
        url = "https://api.chess.com/pub/match/" + shortId
    myrequest = urllib.request.Request(url, None, HEADERS)
    match = {}
    attempts = 0
    while attempts <= 5:
        attempts += 1
        try:
            with urllib.request.urlopen(myrequest, timeout=30) as response:
                resp_bytes = response.read()
                match = json.loads(resp_bytes.decode("utf8"))
                MATCH_DETAILS[shortId] = match
                return match
        except urllib.error.HTTPError as e:
            if e.code == 429:
                print("Sleeping due to HTTP too many requests for ", url,
                      ":", e, file=stderr)
                time.sleep(2)
                continue
            print("Cannot find details for match ", url, e, file=stderr)
            return {}
        except urllib.error.URLError as e:
            if str(e) == "HTTP Error 429: Too Many Requests" or str(e) == "<urlopen error [Errno -3] Temporary failure in name resolution>" :
                print("Sleeping due to URL error for ", url, ":", e, file=stderr)
                time.sleep(2)
                continue
            print("URL error getting details for match - ", url, ":", e, file=stderr)
            return []
        except socket.timeout as e:
            print("Sleeping due to socket timeout for ", url, ":", e, file=stderr)
            time.sleep(5)
            continue
        except http.client.RemoteDisconnected as e:
            print("Sleeping due to RemoteDisconnected", url, ":", e, file=stderr)
            time.sleep(5)
            continue
    print("Cannot find details for match ", url, file=stderr)
    return {}


def match_board(boardurl):
    global MATCH_BOARDS
    if boardurl in MATCH_BOARDS:
        return MATCH_BOARDS[boardurl]
    board = {}
    # now put the api url at the front
    myrequest = urllib.request.Request(boardurl, None, HEADERS)
    try:
        with urllib.request.urlopen(myrequest, timeout=10) as response:
            resp_bytes = response.read()
            board = json.loads(resp_bytes.decode("utf8"))
            MATCH_BOARDS[boardurl] = board
    except urllib.error.HTTPError as e:
        print("Cannot find board ", boardurl, e, file=stderr)
    return board

# return the players from the match in team 'team'.
# team may be 'team1' or 'team2', or the team name, or blank for both
def match_players(matchid, team = "", excludeTeams = set()):
    match = match_details(matchid)
    if not match :
        return set()
    teams = match["teams"]
    team1 = teams["team1"]["name"]
    team2 = teams["team2"]["name"]
    team1url = teams["team1"]["url"]
    team2url = teams["team2"]["url"]
    fairplay_closed1 = teams["team1"]["fair_play_removals"]
    fairplay_closed2 = teams["team2"]["fair_play_removals"]
    # Now which team?
    players = set()
    if (
        (team == team1)
        or (team1url.find(team) > -1)
        or (team == "team1")
        or (team == "")
    ):
        for u in teams["team1"]["players"]:
            if u not in fairplay_closed1:
                players.add(u["username"].lower())
    if (
        (team == team2)
        or (team2url.find(team) > -1)
        or (team == "team2")
        or (team == "")
    ):
        for u in teams["team2"]["players"]:
            if u not in fairplay_closed2:
                players.add(u["username"].lower())
    return players

# return a map of state abbreviation to the chess.com team id for the state
def readClubAbbrevs(fname):
    teams = dict()
    with open(fname, 'r', newline='') as f:
        r = csv.DictReader(f)
        fields = r.fieldnames
        if 'abbr' in fields:
            abbr = 'abbr'
        elif 'State' in fields:
            abbr = 'State'
        else:
            print(f"Cannot find 'abbr' or 'State' columns in {fname}", file=stderr)
            return teams
        if 'url' in fields:
            url = 'url'
        elif 'Link' in fields:
            url = 'Link'
        else:
            print(f"Cannot find 'url' or 'Link' columns in {fname}", file=stderr)
            return teams
        for row in r:
            teams[teamId(row[url])] = row[abbr]
    return teams


def tournament(tourneyid):
    tourney = {}
    url = "https://api.chess.com/pub/tournament/" + matchId(tourneyid)
    myrequest = urllib.request.Request(url, None, HEADERS)
    try:
        with urllib.request.urlopen(myrequest, timeout=30) as response:
            resp_bytes = response.read()
            tourney = json.loads(resp_bytes.decode("utf8"))
    except urllib.error.HTTPError as e:
        print("Cannot find details for tourney ", tourneyid, e, file=stderr)
    return tourney

def tourneyPlayers(tourneyId):
    tourney = tournament(tourneyId)
    players = set()
    for p in tourney['players']:
        players.add(p['username'])
    return players

def describeRating(min_rating, max_rating):
    if min_rating > 0:
        if max_rating < 9999:
            return str(min_rating) + " - " + str(max_rating)
        else:
            return "> " + str(min_rating)
    elif max_rating < 9999:
        return "< " + str(max_rating)
    else:
        return "Open"


def snapshot_match(weburl, num_players=0, match_name=""):
    if num_players == 0 or match_name == "":
        match = match_details(weburl)
        if not match:
            print(f"Cannot find match {weburl}")
            return
        teams = match["teams"]
        players1 = teams["team1"]["players"]
        players2 = teams["team2"]["players"]
        num_players = max(len(players1), len(players2))
        match_name = match["name"]
        weburl = match["url"]
    num_pages = (num_players // 100) + 1
    for snap in range(num_pages):
        page = snap + 1
        today = datetime.today().strftime("%Y-%m-%d")
        filename = match_name + today + "-p" + str(snap + 1) + ".jpg"
        # height is 7500 for a full page of 1000 boards.
        height = 7500
        print("Making snapshot at ", weburl + "/games?p=" + str(page), "height", height)
        if (page * 100) > num_players:
            height = min(height, 200 + int(7500 * ((num_players - (snap * 100)) / 75)))
        if snapshot.snapshot(weburl + "/games?p=" + str(page), filename, height):
            print("Created snapshot", filename, "height", height)

# Report a Match retrieved from the matches API match_details(match) 
# See also reportClubMatches which uses the clubs match API 
def printMatch(matchid, boards=False, snapshot=False, compact=False, matchJson=None, tz="UTC"):
    end = "\t" if compact else "\n"
    if not matchJson:
        matchJson = match_details(matchid)
    if not matchJson:
        print(f"Cannot find match {matchid}")
        return
    teams = matchJson["teams"]
    weburl = matchJson["url"]
    team1 = teams["team1"]["name"]
    team2 = teams["team2"]["name"]
    settings = matchJson["settings"]
    min_rating = settings.get("min_rating", 0)
    max_rating = settings.get("max_rating", 9999)
    min_players = settings.get("min_team_players", 0)
    max_players = settings.get("max_team_players", 9999)

    # TODO do we have "score" : 0  ?
    #   "score":0,"result":"draw"
    #   So test for that combination and it's certain that it's a defaulted match, not a match that ended in a draw.
    # See https://www.chess.com/clubs/forum/view/changed-match-policy-results-in-incorrect-api-end-point-https-api-chess-com-pub-match-id?page=2&newCommentCount=4#comment-100436993
    if matchJson["status"] != "registration" and int(matchJson['boards']) < min_players:
        if not compact:
            print("** Failed to meet minimum number of players!", end=end)
        matchJson["status"] = "aborted due to min players"

    match_name = matchJson["name"]
    print(match_name, end=end)
    print(weburl)
    if not compact:
        print("Status:", matchJson["status"])
    start = matchJson.get("start_time", 0)
    if start > 0:
        startTime = tzDateTime(start, tz=tz)
        if matchJson["status"] == "registration" or matchJson["status"] == "closed":
            print("Starts", startTime, end=end)
        elif matchJson["status"] == "aborted due to min players":
            print("Start date: ", startTime, end=end)
        else:
            print("Started", startTime, end=end)
    if not compact and settings["rules"] != "chess":
        print('Variant: ', settings["rules"])
    print("Boards:", matchJson["boards"], end=end)
    if min_players > 0:
        print("Min players:", min_players, end=end)
    if max_players < 9999:
        print("Max players:", max_players, end=end)
    # if matchJson["status"] != "aborted due to min players":
    print("Rating:", describeRating(min_rating, max_rating), end=end)

    # get ratings for players if missing (only included in registration phase)
    players1 = teams["team1"]["players"]
    players2 = teams["team2"]["players"]
    variant = settings["rules"]

    # only matches in registration list the ratings here, so add in ratings if missing
    if boards and not compact:
        for p1, p2 in itertools.zip_longest(players1, players2):
            for p in (p1, p2):
                if p and "rating" not in p:
                    ustats = stats(p["username"])
                    if ustats:
                        p["rating"] = rating(ustats, variant)

    have_ratings = False
    if len(players1) > 0 and "rating" in players1[0]:
        players1 = sorted(players1, key=lambda x: x.get("rating", 0), reverse=True)
        have_ratings = True
    if len(players2) > 0 and "rating" in players2[0]:
        players2 = sorted(players2, key=lambda x: x.get("rating", 0), reverse=True)
        have_ratings = True

    rating_exceeded1 = []
    rating_exceeded2 = []
    if have_ratings:
        for p in players1:
            if p["rating"] > max_rating:
                rating_exceeded1.append(p)
        for p in rating_exceeded1:
            players1.remove(p)
        for p in players2:
            if p["rating"] > max_rating:
                rating_exceeded2.append(p)
        for p in rating_exceeded2:
            players2.remove(p)

    have_results = "result" in teams["team1"] and int(matchJson['boards']) >= min_players
    have_score = (
        "score" in teams["team1"]
        and matchJson["status"] != "registration"
        and matchJson["status"] != "closed"
        and not compact
        and int(matchJson['boards']) >= min_players
    )
    strength = 0
    adv1 = 0
    adv2 = 0
    team1_wins = 0
    team2_wins = 0
    timeout_players1 = []
    timeout_players2 = []
    closed1 = []
    closed2 = []
    fairplay_closed1 = teams["team1"]["fair_play_removals"]
    fairplay_closed2 = teams["team2"]["fair_play_removals"]

    for board in range(min(len(players1), len(players2))):
        u1 = players1[board]
        u2 = players2[board]
        rating1 = u1.get("rating", 0)
        rating2 = u2.get("rating", 0)

        if board < max_players:
            strength += rating1 - rating2
            if rating1 > rating2:
                adv1 += 1
            if rating2 > rating1:
                adv2 += 1

        if u1.get("status", "") == "closed" or u1.get("status", "") == "closed:fair_play_violations":
            closed1.append(u1["username"])
        if u2.get("status", "") == "closed" or u2.get("status", "") == "closed:fair_play_violations":
            closed2.append(u2["username"])

        if u1.get("played_as_white", "") == "win":
            team1_wins += 1
        if u1.get("played_as_black", "") == "win":
            team1_wins += 1
        if u2.get("played_as_white", "") == "win":
            team2_wins += 1
        if u2.get("played_as_black", "") == "win":
            team2_wins += 1

    if have_results:
        team1_result = teams["team1"].get("result", "playing")
        print("   ", team1, team1_wins, "wins,", "overall", team1_result, end="\t")
        team2_result = teams["team2"].get("result", "playing")
        print("", team2, team2_wins, "wins,", "overall", team2_result)
    elif have_score:
        team1_result = teams["team1"].get("score", 0)
        print("   ", team1, team1_wins, "wins,", "score", team1_result, end="\t")
        team2_result = teams["team2"].get("score", 0)
        print("", team2, team2_wins, "wins,", "score", team2_result)
    elif not compact:
        print("   ", team1, len(players1), "players", end="\t\t")
        print("", team2, len(players2), "players")
    if matchJson["status"] != "registration" and int(matchJson['boards']) < min_players:
        if len(players1) < min_players:
            print("   ", team1, 'failed to reach min number of players')
        if len(players2) < min_players:
            print("   ", team2, 'failed to reach min number of players')
        if len(players1) < min_players and len(players2) < min_players:
            print("   ", 'Double forfeit!')

    if have_ratings and matchJson["status"] != "aborted due to min players":
        print("Advantage :", str(adv1).rjust(5), "\t\t\t", str(adv2).rjust(5))
        print(
            "Relative Strength =",
            strength,
            "\t",
            "Leading:",
            team1 if strength > 0 else team2,
        )
    # result = match.get('result', "")
    if fairplay_closed1 != []:
        print(team1, "closed for fair play", "@" + " @".join(fairplay_closed1))
    if fairplay_closed2 != []:
        print(team2, "closed for fair play", "@" + " @".join(fairplay_closed2))

    if boards:
        for board in range(max(len(players1), len(players2))):
            u1 = players1[board] if board < len(players1) else ""
            u2 = players2[board] if board < len(players2) else ""
            if board == min_players:
                print("----------------------- MIN PLAYERS REACHED ----------------------------")
            if board == max_players:
                print("----------------------- MAX PLAYERS CUTOFF -----------------------------")
            if have_ratings:
                rating1 = rating2 = 0
                to1 = to2 = 0
                if u1 != "":
                    rating1 = u1.get("rating", 0)
                    to1 = u1.get("timeout_percent", 0)
                if u2 != "":
                    rating2 = u2.get("rating", 0)
                    to2 = u2.get("timeout_percent", 0)

                if to1 > 15:
                    timeout_players1.append((board, u1["username"], to1))
                if to2 > 15:
                    timeout_players2.append((board, u2["username"], to2))

                if (u1 == "") or (u2 == ""):
                    marker1 = " "
                    marker2 = " "
                elif rating1 > rating2+100:
                    marker1 = "+++"
                    marker2 = " "
                elif rating1 > rating2+50:
                    marker1 = "++"
                    marker2 = " "
                elif rating1 > rating2:
                    marker1 = "+"
                    marker2 = " "
                elif rating1 == rating2:
                    marker1 = "="
                    marker2 = "="
                elif rating1+100 < rating2:
                    marker1 = " "
                    marker2 = "+++"
                elif rating1+50 < rating2:
                    marker1 = " "
                    marker2 = "++"
                else:  # (rating1 > 0) and (rating2 > 0):
                    marker1 = " "
                    marker2 = "+"
                if u1 == "":
                    print(str(board + 1).rjust(3), " ".ljust(18), "\t", end="\t\t")
                else:
                    print(
                        str(board + 1).rjust(3),
                        "@" + u1["username"].ljust(18),
                        str(rating1).rjust(5),
                        marker1,
                        end=" \t",
                    )
                if u2 == "":
                    print()
                else:
                    print(
                        "@" + u2["username"].ljust(18), str(rating2).rjust(5), marker2
                    )
            if have_results or have_score:
                wins1 = 0
                wins2 = 0
                if u1 != "" and u1.get("played_as_white", "") == "win":
                    wins1 += 1
                if u1 != "" and u1.get("played_as_black", "") == "win":
                    wins1 += 1
                if u2 != "" and u2.get("played_as_white", "") == "win":
                    wins2 += 1
                if u2 != "" and u2.get("played_as_black", "") == "win":
                    wins2 += 1
                print(str(board + 1).rjust(3), end=' ')
                if u1 != "":
                    print("@" + u1["username"].ljust(18), str(wins1).rjust(5), "wins", end="\t")
                else:
                    print(end='\t')
                if u2 != "":
                    print("@" + u2["username"].ljust(18), str(wins2).rjust(5), "wins")
                else:
                    print()

    if rating_exceeded1 != []:
        print(team1, "rating exceeded: ", ', '.join([f"@{u['username']} ({u['rating']})" for u in rating_exceeded1]))
    if rating_exceeded2 != []:
        print(team2, "rating exceeded:", ', '.join([f"@{u['username']} ({u['rating']})" for u in rating_exceeded2]))
    if timeout_players1 != []:
        print("Players with > 15% timeout in", team1)
        for board, u, to in timeout_players1:
            print(str(board + 1).rjust(3), "@" + u.ljust(18), str(to).rjust(5) + "%")
    if timeout_players2 != []:
        print("Players with > 15% timeout in", team2)
        for board, u, to in timeout_players2:
            print(str(board + 1).rjust(3), "@" + u.ljust(18), str(to).rjust(5) + "%")

    # now take snapshots.  up to 100 boards per page
    if snapshot and (matchJson["status"] == "registration" or matchJson["status"] == "closed"):
        num_players = max(len(players1), len(players2))
        snapshot_match(weburl, num_players=num_players, match_name=match_name)


def country_players(country):
    members = {}
    url = "https://api.chess.com/pub/country/" + country + "/players"
    myrequest = urllib.request.Request(url, None, HEADERS)
    try:
        with urllib.request.urlopen(myrequest, timeout=120) as response:
            resp_bytes = response.read()
            members = json.loads(resp_bytes.decode("utf8"))
    except urllib.error.HTTPError as e:
        print("Cannot find members for country ", country, e, file=stderr)
    return members


def ruleStats(ustats, rules="chess"):
    if rules == "chess" and "chess_daily" in ustats:
        return ustats["chess_daily"]
    elif rules == "chess960" and "chess960_daily" in ustats:
        return ustats["chess960_daily"]
    elif rules == "blitz" and "chess_blitz" in ustats:
        return ustats["chess_blitz"]
    elif rules == "rapid" and "chess_rapid" in ustats:
        return ustats["chess_rapid"]
    elif rules == "bullet" and "chess_bullet" in ustats:
        return ustats["chess_bullet"]
    elif rules in ustats:
        return ustats[rules]
    return None


def all_ratings(ustats):
    max = 0
    all = {}
    for rules in ["chess", "chess960", "blitz", "rapid", "bullet"]:
        rate = rating(ustats, rules)
        all[rules] = rate
    return all

def max_rating(ustats):
    max = 0
    for rules in ["chess", "chess960", "blitz", "rapid", "bullet"]:
        rate = rating(ustats, rules)
        if rate > max:
            max = rate
    return max

def rating(ustats, rules="chess"):
    rs = ruleStats(ustats, rules)
    if rs and "last" in rs:
        return rs["last"].get("rating", 0)
    return 0

def rating960(ustats):
    return rating(ustats, rules="chess960")


# completed_games: returns 3-tuple: (win, loss, draw)
def completed_games(ustats, rules="chess"):
    wins = 0
    losses = 0
    draws = 0
    record = None
    rs = ruleStats(ustats, rules)
    if rs:
        record = rs.get("record", None)
    if record:
        wins = record["win"]
        losses = record["loss"]
        draws = record["draw"]
    return (wins, losses, draws)


def num_completed_games(ustats, rules="chess"):
    if not ustats:
        return 0
    win, loss, draw = completed_games(ustats, rules)
    return win + loss + draw


def games_won_percent(ustats, rules="chess"):
    if not ustats:
        return 0
    win, loss, draw = completed_games(ustats, rules)
    if win + loss + draw == 0:
        return 0
    return (win / (win + loss + draw)) * 100


def timeout_ratio(ustats, rules="chess"):
    if not ustats:
        return 0
    rs = ruleStats(ustats, rules)
    if rs and "record" in rs:
        return rs["record"].get("timeout_percent", 0)
    return 0


def glickoRD(ustats, rules="chess"):
    if not ustats:
        return 0
    # return ruleStats(ustats, rules)["last"]["rd"]
    rs = ruleStats(ustats, rules)
    if rs and "last" in rs:
        return rs["last"].get("rd", 350)
    return 350


# last_logon: how many hours ago were they last online?
def last_logon(user):
    if type(user) == str:
        details = user_details(user)
    else:
        details = user
    if not details:
        print("last_logon(): User ", user, "not found!", file=stderr)
        return 0
    online = details["last_online"]
    now = int(time.time())
    hours = (now - online) / 3600
    # print (user, 'online', online, 'now', now, 'hours = ', hours)
    return hours


# member_since: how many days ago did they join chess.com?
def member_since(user):
    if type(user) == str:
        details = user_details(user)
    else:
        details = user
    if not details:
        return 0
    online = details["joined"]
    now = int(time.time())
    hours = (now - online) / 3600
    days = hours / 24
    # print (user, 'online', online, 'now', now, 'hours = ', hours)
    return days


def printStats(user, ustats, rules="chess", printLoc=False, printCountry=False, joined=0, clubs=False, adminClubs=False):
    details = user_details(user) if type(user) == str else user
    if not details:
        print("printStats(): User ", user, "not found!", file=stderr)
    else:
        rting = rating(ustats, rules)
        max_rting = max_rating(ustats)
        # rting960 = rating960(ustats)
        daily_TO = timeout_ratio(ustats, rules)
        num_games = num_completed_games(ustats, rules)
        name = details.get("name", "")
        title = details.get("title", "")
        country = details.get("country", "")  if printCountry else ""  # TODO strip off URL
        u = details["username"]
        location = details.get("location", "<>") if printLoc else ""
        status = details.get("status", "")
        printUserInfo(u, title, name, rting, max_rting, daily_TO, num_games=num_games, 
                      location=location, country=country, joined=joined, 
                      clubs=clubs, adminClubs=adminClubs, status=status)

def countryName(ccode):
    return country.countryName(ccode)

def stateName(code):
    return states.abbrev_to_us_state[code]

def stateAbbrev(name):
    return states.abbrev_to_us_state[name]
    
def printUserInfo(username, title, name, rating, max_rating, daily_TO, num_games=None, location="", country="", joined=0, nsu={}, clubs=False, adminClubs=False, status=""):
    if title != "" and not title.startswith(" "):
        title = " " + title
    if country:
        country = countryName(country)
    username = stduser(username)
    print(str("@" + username + title).ljust(18), str('"' + html.unescape(name) + '"').ljust(20), end="")
    if location != "":
        print(html.unescape(location).ljust(16), end="")
    if country != "":
        # Get the country name
        print(" ", html.unescape(country).ljust(12), end="")
    print(str(rating).rjust(5), end="")
    if max_rating > 0:
        print(' Max', str(max_rating).rjust(5), end="")
    if num_games:
        print(' Played', str(num_games).rjust(5), end="")
    if joined > 0:
        print(" jnd", date.fromtimestamp(joined), end="")
    for n in nsu:
        print(" ", n, end="")
    if daily_TO >= 5.0:
        print(" ", daily_TO, "T/O%", end="")
    if status != "" and status != "basic":
        print(f" {status}", end="")
    print(flush=True)
    if clubs:
        reportClubs(username) 
    if adminClubs:
        reportAdminClubs(username)


def printHeaderCSV(keys=[]):
    print(
        "Username",
        "Title",
        "Name",
        "Rating",
        "TO%",
        "Location",
        "Date Joined",
        keys,
        sep=",",
    )


def printUserInfoCSV(username, title, name, rating, daily_TO, location="", joined=0, nsu={}):
    j = date.fromtimestamp(joined) if joined > 0 else ""
    print(
        "@" + username,
        title,
        '"' + name + '"',
        rating,
        daily_TO,
        '"' + location + '"',
        j,
        nsu,
        sep=",",
        flush=True,
    )


# Checks the user by location only
def checkUser(username, location_pattern):
    detail = user_details(username)
    if detail:
        if "location" in detail:
            loc = detail["location"]
            if location_pattern.match(loc):
                stat = stats(username)
                printStats(detail, stat, printLoc=True)
    else:
        print("checkUser(): User ", username, "not found!", file=stderr)


def report(username, location=False):
    user = user_details(username)
    if user:
        ustats = stats(username)
        printStats(user, ustats, printLoc=location, printCountry=location)
        return True
    else:
        print("report(): User ", username, "not found!", file=stderr)
        return False

def reportClubs(username, verbose=False):
    clubs = getClubs(username)
    if not clubs:
        return
    if verbose:
        print("\t", len(clubs), "Clubs: ")
        for c in clubs:
            print("  ", html.unescape(c["name"]), "Joined", date.fromtimestamp(c["joined"]))
    else:
        print("\t", len(clubs), "Clubs: ", end="")
        for c in clubs:
            print(html.unescape(c["name"]), end=", ")
        print()


def histogram(stats):  # stats is a dict by username
    hist = dict()
    # Group ratings by century
    for u in stats:
        rting = rating(stats[u])
        binnum = rting // 100
        if binnum in hist:
            hist[binnum] += 1
        else:
            hist[binnum] = 1
    print("\nMember count:")
    pawn = "\u2659"
    for binnum in sorted(hist, reverse=True):
        min = binnum * 100
        max = ((binnum + 1) * 100) - 1
        count = hist[binnum]
        print(
            str(min).rjust(4),
            "-",
            str(max).rjust(4),
            ":",
            str(count).rjust(3),
            " ",
            pawn * (count // 10),
        )

def countryHistogram(players):  # players is a set/list of usernames
    hist = dict()
    # Group countries
    for u in players:
        details = user_details(u)
        country = details['country']
        if country in hist:
            hist[country] += 1
        else:
            hist[country] = 1
    print("\nCount by country:")
    # pawn = "\u2659"
    for country in hist: # sorted(hist, reverse=True):
        count = hist[country]
        print(
            countryName(country).ljust(12),
            ":",
            str(count).rjust(3)
        )

def testCriterion(value, criterion):
    # criterion needs to be pairs of operator and value
    c = criterion.strip("'").strip('"').split()
    # print(value, criterion, c)
    if len(c) >= 2:
        # print(value, c[0], c[1])
        if c[0] == "<":
            if not value < float(c[1]):
                return False
        if c[0] == "<=":
            if not value <= float(c[1]):
                return False
        if c[0] == ">":
            if not value > float(c[1]):
                return False
        if c[0] == ">=":
            if not value >= float(c[1]):
                return False
    if len(c) >= 4:
        if c[2] == "<":
            if not value < float(c[3]):
                return False
        if c[2] == "<=":
            if not value <= float(c[3]):
                return False
        if c[2] == ">":
            if not value > float(c[3]):
                return False
        if c[2] == ">=":
            if not value >= float(c[3]):
                return False
    return True

def matchesStats(u, criteria, verbose=False, ratingRules="any"):
    ustats = stats(u)
    if not ustats:
        return False
    details = user_details(u)
    if not details:
        print("matchesStats(): User ", u, "not found!", file=stderr)
        return False
    ok = True
    for key in criteria:
        if key == "rating":
            rting = max_rating(ustats) if ratingRules == "any" else rating(ustats, ratingRules)
            criterion = testCriterion(rting, criteria[key])
            ok &= criterion
            if verbose and not criterion:
                print(u, "FAILED rating", rting, criteria[key])
        elif key == "min_rating":
            rting = max_rating(ustats) if ratingRules == "any" else rating(ustats, ratingRules)
            criterion = testCriterion(rting, "> " + criteria[key])
            ok &= criterion
            if verbose and not criterion:
                print(u, "FAILED min rating", rting, criteria[key])
        elif key == "max_rating":
            rting = max_rating(ustats) if ratingRules == "any" else rating(ustats, ratingRules)
            criterion = testCriterion(rting, "<= " + criteria[key])
            ok &= criterion
            if verbose and not criterion:
                print(u, "FAILED max rating", rting, criteria[key])
        elif key == "timeout_ratio" or key == "timeout":
            criterion = testCriterion(timeout_ratio(ustats), criteria[key])
            ok &= criterion
            if verbose and not criterion:
                print(u, "FAILED timeout", timeout_ratio(ustats), criteria[key])
        elif key == "clubs" or key == "num_clubs":
            criterion = testCriterion(num_clubs(u), criteria[key])
            ok &= criterion
            if verbose and not criterion:
                print(u, "FAILED num clubs", num_clubs(u), criteria[key])
        elif key == "games" or key == "num_games":
            criterion = testCriterion(num_games(u), criteria[key])
            ok &= criterion
            if verbose and not criterion:
                print(u, "FAILED num games", num_games(u), criteria[key])
        elif key == "last_logon" or key == "last_login":
            # this is how many hours ago they were last online
            criterion = testCriterion(last_logon(details), criteria[key])
            ok &= criterion
            if verbose and not criterion:
                print(u, "FAILED last login", last_logon(details), criteria[key])
        elif key == "games_completed" or key == "num_games_completed":
            criterion = testCriterion(num_completed_games(ustats), criteria[key])
            ok &= criterion
            if verbose and not criterion:
                print(u, "FAILED num completed games", num_completed_games(ustats), criteria[key])
        elif key == "games_won_percent" or key == "num_games_won_percent":
            criterion = testCriterion(games_won_percent(ustats), criteria[key])
            ok &= criterion
            if verbose and not criterion:
                print(u, "FAILED won percent", games_won_percent(ustats), criteria[key])
        elif key == "glicko_rd" or key == "glicko-RD":
            criterion = testCriterion(glickoRD(ustats), criteria[key])
            ok &= criterion
            if verbose and not criterion:
                print(u, "FAILED Glicko RD", glickoRD(ustats), criteria[key])
        elif key == "member" or key == "member_since":
            # this is how many days ago they joined chess.com
            criterion = testCriterion(member_since(details), criteria[key])
            ok &= criterion
            if verbose and not criterion:
                print(u, "FAILED member_since", member_since(details), criteria[key])
        if not verbose and not ok:
            break
    if verbose and ok:
        print(u, "meets all recruitment criteria")
    return ok


def reportEligiblePlayers(eligible, verbose=False, sort=True):
    if sort:
        if verbose:
            print("..sorting.. list size is", len(eligible))
        eligible.sort(key=itemgetter(1), reverse=True)
    # write the user info banded by 100-rating range
    if len(eligible) > 0:
        lastrating = eligible[0][1]
        for (u, rating, stats) in eligible:
            if sort and (lastrating // 100) != (rating // 100):
                print()
            printStats(u, stats, printLoc=verbose, adminClubs=verbose)
            lastrating = rating

    # write the user names again, simply without additional info, in groups of 30
    if len(eligible) > 0:
        names = ""
        count = 0
        start = eligible[0][1]
        lastrating = start
        for (u, rating, stats) in eligible:
            if count == 30:
                if sort:
                    print("\n{} to {}:".format(start, lastrating))
                print()
                print(names)
                names = ""  # reset
                count = 0
                start = rating
            if count > 0:
                names += " "
            names += '@' + u
            count += 1
            lastrating = rating
        if count > 0:
            if sort:
                print("\n{} to {}:".format(start, eligible[-1][1]))
            print()
            print(names)

def reportDivisions(username, divisions, eol="\n"):
    # Report if member of division team
    playerClubs = set()
    for c in getClubs(username):
        playerClubs.add(teamId(c['url']))
    foundTeams = 0
    for div in divisions:
        for team in divisions[div]:
            # print(f"Checking {team} == {playerClub}")
            if team in playerClubs:
                foundTeams += 1
                if isAdmin(username, team):
                    print(f' League Member - Div {div}: {team} (ADMIN)', end=eol)
                else:
                    print(f' League Member - Div {div}: {team}', end=eol)
    if foundTeams == 0:
        print(' Not in any division team!', end=eol)


def fixCache(cache):
    recov = {}
    try:
        for k in cache:
            recov[k] = cache[k]
    except Exception:
        pass
    return recov

def restoreCache(recov, cache):
    for k in recov:
        cache[k] = recov[k]

def fixAll():
    global USER_DETAILS_CACHE
    recov = fixCache(USER_DETAILS_CACHE)
    os.remove(os.path.join(CACHEDIR, "details.shelve.db"))
    USER_DETAILS_CACHE = shelve.open(os.path.join(CACHEDIR, "details.shelve"), flag="c", protocol=None, writeback=False)
    restoreCache(recov, USER_DETAILS_CACHE)

    global USER_STATS_CACHE
    recov = fixCache(USER_STATS_CACHE)
    os.remove(os.path.join(CACHEDIR, "stats.shelve.db"))
    USER_STATS_CACHE = shelve.open(os.path.join(CACHEDIR, "stats.shelve"), flag="c", protocol=None, writeback=False)
    restoreCache(recov, USER_STATS_CACHE)

    global USER_CLUBS_CACHE
    recov = fixCache(USER_CLUBS_CACHE)
    os.remove(os.path.join(CACHEDIR, "userclubs.shelve.db"))
    USER_CLUBS_CACHE = shelve.open(os.path.join(CACHEDIR, "userclubs.shelve"), flag="c", protocol=None, writeback=False)
    restoreCache(recov, USER_CLUBS_CACHE)

    global USER_GAMES_CACHE
    recov = fixCache(USER_GAMES_CACHE)
    os.remove(os.path.join(CACHEDIR, "games.shelve.db"))
    USER_GAMES_CACHE = shelve.open(os.path.join(CACHEDIR, "games.shelve"), flag="c", protocol=None, writeback=False)
    restoreCache(recov, USER_GAMES_CACHE)

    global TEAMS_CACHE
    recov = fixCache(TEAMS_CACHE)
    os.remove(os.path.join(CACHEDIR, "teams.shelve.db"))
    TEAMS_CACHE = shelve.open(os.path.join(CACHEDIR, "teams.shelve"), flag="c", protocol=None, writeback=False,)
    restoreCache(recov, TEAMS_CACHE)
    

# fixAll()

