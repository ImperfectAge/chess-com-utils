#!/usr/bin/env python3

import sys
from sys import stderr
from datetime import date
import chess


if  len(sys.argv) <= 1 :
    print("Usage:", sys.argv[0], "player [player...]", file=stderr)
else:
    chess.refresh()     # Don't use the cache.
    criteria = chess.configCriteria()
    team = None
    divisions = {}           # teams in each division
    cfg = chess.readConfig()
    if "divisions" in cfg:
        for div in cfg['divisions']:
            divisions[div] = [x.strip() for x in cfg['divisions'][div].split(',')]
    team = chess.homeTeam()

    for username in sys.argv[1:]  :
        if username[0] == '@':
            username = username[1:]
        username = username.lower()
        chess.report(username, location=True)
        if team :
            member = chess.isMember(username, team)
            if member:
                joined = chess.joinedDate(username, team)
                jnd = date.fromtimestamp(joined)
                print(f" Member of {team} - joined {jnd}")
            else:
                print(f" NOT a member of {team}")

        # Reporting on league divisions
        if divisions:
            chess.reportDivisions(username, divisions)
        chess.reportClubs(username)
        # Report any admin positions in those clubs
        chess.reportAdminClubs(username)
        stats = chess.stats(username)
        if stats:
            print("Glicko RD: ", chess.glickoRD(stats))
            print("Num current games: ", chess.num_games(username))
            print("Num completed games: ", chess.num_completed_games(stats))
            print("Games won percent: ", chess.games_won_percent(stats))
            print("Last logon (hours): %.2f" % chess.last_logon(username))
            if criteria:
                matches = chess.matchesStats(username, criteria, verbose=True)
                print("Matches criteria: ", end='')
                print(matches)
