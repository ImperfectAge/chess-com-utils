#!/usr/bin/env python3

import sys
from sys import stderr
from time import sleep

import chess

teamMembers = set()
sourceTeam = ""

def find_players(matches) :
    criteria = chess.configCriteria()
    if criteria == {}:
        print("Failed to find config.txt")
        exit(1)

    team = chess.homeTeam()

    if sourceTeam != "":
        print("..looking for members of {} in below matches".format(sourceTeam))
    else:
        print("..looking for all players from below matches")
    print("..excluding members of {}".format(team))
    teamMembers = chess.members(team)
    eligible = []   # we will use this for sorting by rating

    seen = set()
    # initialize seen from previously invited list
    try:
        with open("invited.txt", "r") as f:
            for line in f:
                # need to split each line .. and add all into 'seen'
                for name in line.strip().split():
                    seen.add(chess.stduser(name.strip()))
        print("..excluding {} previously-invited players".format(len(seen)))
    except:
        print("invited.txt not found, not excluding previously-invited names..")

    # we should only calculate the excluded team and users once
    exclude_users = chess.getExcludedPlayers(criteria['exclude'] if 'exclude' in criteria else "")
    exclude_clubs = chess.stringToSet(criteria['exclude-teams'] if 'exclude-teams' in criteria else "")

    for match in matches :
        #print(match)
        chess.printMatch(match, compact=True)
        print()
        # pass "" for the team to get match players from both teams
        match_players = chess.match_players(match, sourceTeam)
        for u in match_players:
            if u not in seen and u not in teamMembers and u not in exclude_users and not chess.isMemberOfAny(exclude_clubs):
                seen.add(u)
                if chess.matchesStats(u, criteria) :
                    stats = chess.stats(u)
                    rating = chess.rating(stats, "chess")
                    # put into list of 3-tuples for sorting
                    eligible.append((u, rating, stats))

    print("*** Recruitment candidates for", team, "***")
    chess.reportEligiblePlayers(eligible)

chess.enableCache()
if (len(sys.argv) < 2):
    print("Usage:", sys.argv[0], "[-team source-team-name] match-id { match-id... } ", file=stderr)
elif sys.argv[1] == '-team':
    sourceTeam = sys.argv[2]
    find_players(sys.argv[3:])
else:
    find_players(sys.argv[1:])

