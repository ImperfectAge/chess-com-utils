#!/usr/bin/env python3

import urllib
#import urllib.parse as parse
import urllib.request
import urllib.error as error
import requests
import json
import base64
import sys
import os
import errno


# Rendertron Post.  Using local service running on port 3000
def snapshot(site, imagePath, height=2000) :
    api = 'http://127.0.0.1:3000/screenshot/'
    user_agent = "Mozilla/5.0 (X11; CrOS x86_64 12607.58.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.86 Safari/537.36"
    headers = {'User-Agent': user_agent}
    json = {"options" : {
        "headers" : headers,
        "fullPage" : "true",
        "timeout": "3000000"
        }}
    safe_site = urllib.parse.quote(site, safe='~()*!.\'')
    response = requests.post(api + safe_site + '?isMobile=true&height=' + str(height), json=json, headers=headers, stream=True)
    # save file, see https://stackoverflow.com/a/13137873/7665691
    if response.status_code == 200:
        with open(imagePath, 'wb') as file:
            for chunk in response:
            #for chunk in response.iter_content(1024):
                file.write(chunk)
        return True
    else:
        print("Failed to request", site, "; response was", response.status_code, response.text)
        return False

# Rendertron Get - for RENDER.  Using local service running on port 3000
def render(site, imagePath, height=2000) :
    api = 'http://127.0.0.1:3000/render/'
    user_agent = "Mozilla/5.0 (X11; CrOS x86_64 12607.58.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.86 Safari/537.36"
    headers = {'User-Agent': user_agent}
    safe_site = urllib.parse.quote(site, safe='~()*!.\'')
    response = requests.get(api + safe_site + '?isMobile=true&height=' + str(height), headers=headers, stream=True)
    # save file, see https://stackoverflow.com/a/13137873/7665691
    if response.status_code == 200:
        with open(imagePath, 'wb') as file:
            for chunk in response:
            #for chunk in response.iter_content(1024):
                file.write(chunk)
        return True
    else:
        print("Failed to request", site, "; response was", response.status_code, response.text)
        return False

# Rendertron Post using Demo service at render-tron.appspot.com
def snapshot_demo(site, imagePath, height=2000) :
    api = 'https://render-tron.appspot.com/screenshot/'
    user_agent = "Mozilla/5.0 (X11; CrOS x86_64 12607.58.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.86 Safari/537.36"
    headers = {'User-Agent': user_agent}
    json = {"options" : {
        "headers" : headers,
        "fullPage" : "true"
        }}
    safe_site = urllib.parse.quote(site, safe='~()*!.\'')
    response = requests.post(api + safe_site + '?isMobile=true&height=' + str(height), json=json, headers=headers, stream=True)
    # save file, see https://stackoverflow.com/a/13137873/7665691
    if response.status_code == 200:
        with open(imagePath, 'wb') as file:
            for chunk in response:
            #for chunk in response.iter_content(1024):
                file.write(chunk)
        return True
    else:
        print("Failed to request", site, "; response was", response.status_code, response.text)
        return False

# Rendertron Get using Demo service at render-tron.appspot.com
def snapshot_get(site, imagePath) :
    api = 'https://render-tron.appspot.com/screenshot/'
    # height = 7500
    height = 500
    user_agent = "Mozilla/5.0 (X11; CrOS x86_64 12607.58.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.86 Safari/537.36"
    headers = {'User-Agent': user_agent}
    safe_site = urllib.parse.quote(site, safe='~()*!.\'')
    response = requests.get(api + safe_site + '?height=' + str(height), headers=headers, stream=True)
    #response = requests.get(api + safe_site, stream=True)
    # save file, see https://stackoverflow.com/a/13137873/7665691
    if response.status_code == 200:
        with open(imagePath, 'wb') as file:
            for chunk in response:
            #for chunk in response.iter_content(1024):
                file.write(chunk)
    else:
        print("Failed to request", site, "; response was", response.status_code, response.text)


def snapshot_g(site, imagePath) :
    #   The Google API.  Remove "&strategy=mobile" for a desktop screenshot
    #api = "https://www.googleapis.com/pagespeedonline/v1/runPagespeed?screenshot=true&strategy=mobile&url=" + urllib.parse.quote(site)
    api = "https://www.googleapis.com/pagespeedonline/v1/runPagespeed?screenshot=true&url=" + urllib.parse.quote(site)

    #   Get the results from Google
    myrequest = urllib.request.Request(site)
    site_data = ""
    try:
        with urllib.request.urlopen(myrequest,timeout=20) as response:
            response_bytes = response.read()
            site_data = json.loads(response_bytes.decode('utf8'))
    except urllib.error.URLError as e:
        print("Unable to retreive data at", site)
        sys.exit()

    try:
        screenshot_encoded =  site_data['screenshot']['data']
    except ValueError:
        print("Invalid JSON encountered.")
        sys.exit()

    #   Google has a weird way of encoding the Base64 data
    screenshot_encoded = screenshot_encoded.replace("_", "/")
    screenshot_encoded = screenshot_encoded.replace("-", "+")
    #   Decode the Base64 data
    screenshot_decoded = base64.b64decode(screenshot_encoded)

    direct = os.path.dirname(imagePath)
    if (direct == ""): direct = os.getcwd()
    if not os.path.exists(direct):
        try:
            os.makedirs(direct)
        except  OSError as exc:
            if exc.errno  != errno.EEXIST:
                raise
    #   Save the file
    with open(imagePath, 'wb') as file_:
        file_.write(screenshot_decoded)
        print("Screenshot written to ", imagePath)

def main(argv) :
    #   The website's URL as an Input
    site = argv[1]
    imagePath = argv[2]
    hgt = 7500
    if (len(argv) > 3) :
        hgt = argv[3]
    snapshot(site, imagePath, hgt)
    #snapshot_g(site, imagePath)

if __name__ == "__main__" :
    main(sys.argv)

