#!/bin/bash
# Leaderboard Names.  Extract player names from the chess.com leaderboard using
# rendertron and pup.  Rendertron is expected running on localhost:3000
# For pup, see https://github.com/ericchiang/pup
#
# Usage: $0 <page-number>
#   e.g. $0 246 
# https://www.chess.com/leaderboard/daily?page=246
#
if [[ $# -ne 1 ]]; then
    echo "$0: Illegal number of parameters"
    echo "Usage: $0 <page-number>
    echo "  e.g. $0 246 
    echo "Would extract names from: https://www.chess.com/leaderboard/daily?page=246"
    exit 2
fi
echo "leaderboard page: $1" >&2 
curl -s http://localhost:3000/render/https://www.chess.com/leaderboard/daily%3Fpage%3D$1 | pup '.user-username-blue text{}'
