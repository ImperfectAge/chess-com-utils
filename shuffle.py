#!/usr/bin/env python3

from random import shuffle
import sys

names = sys.argv[1:]
shuffle(names)
i=0
while(i<len(names)):
    print(names[i], names[i+1])
    i+=2
