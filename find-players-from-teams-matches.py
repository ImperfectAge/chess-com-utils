#!/usr/bin/env python3

# Find potential players from team matches from a team
# We are interested in the players that are now no longer in that team
# but who previously played in matches

import sys
from datetime import datetime
from datetime import timedelta 
from sys import stderr
from time import sleep

import chess



def main(argv) :
    criteria = chess.configCriteria()
    if criteria == {}:
        print("Failed to find config.txt, or criteria section")
        exit(1)

    excludeCurrent = False
    verbose = False
    refresh = False
    teams = []
    argc = 1
    while argc < len(argv):
        if argv[argc] == "--exclude-current":
            excludeCurrent = True
            argc += 1
        elif argv[argc] == "--verbose":
            verbose = True
            argc += 1
        elif argv[argc] == "--refresh":
            refresh = True
            argc += 1
        elif argv[argc] == "--min":
            criteria['rating'] = ">= " + argv[argc+1]
            argc += 2
        elif argv[argc] == "--max":
            criteria['rating'] = "<= " + argv[argc+1]
            argc += 2
        else:
            teams.append(argv[argc])

    if refresh:
        chess.refresh()

    print("[criteria]")
    chess.printConfigSection(criteria)

    target_team = chess.homeTeam()

    invited = set()
    # initialize seen from previously invited list
    try:
        with open("invited.txt", "r") as f:
            for line in f:
                # need to split each line .. and add all into 'seen'
                for name in line.strip().split():
                    invited.add(chess.stduser(name.strip()))
    except:
        print("invited.txt not found, not excluding previously-invited names..")

    players = set()
    admins = set()
    for team in teams:
        team = chess.teamId(team)
        if not chess.clubDetails(team):
            continue
        chess.printClubDetails(team)

        HOW_OLD_MATCHES = 2*365                 # Start date 2 years ago
        startDate = datetime.now() - timedelta(days=HOW_OLD_MATCHES)
        # Get the club players from completed and inprogress games
        # If allTeams is true, we would get players from this club and thier opponents
        teamPlayers = chess.clubMatchPlayers(team, ["in_progress", "finished"], allTeams=False,start=startDate.timestamp())
        print(f"..found {len(teamPlayers)} players for {team}", file=stderr)
        if excludeCurrent:
            # Remove all the current members (if a private team this would return same as above, so we say fromMatches=False)
            teamMembers = chess.members(team, fromMatches=False)
            print(f"..excluding {len(teamMembers)} current members of {team}", file=stderr)
            teamPlayers -= teamMembers
        else:
            # we will remove only the team admins 
            teamAdmins = chess.admins(team)
            print(f"..excluding {len(teamAdmins)} admins of {team}", file=stderr)
            admins |= teamAdmins
        players |= teamPlayers

    players -= admins
    print(f"..excluding members of {target_team}", file=stderr)
    if invited:
        print(f"..excluding {len(invited)} previously-invited players", file=stderr)
        players -= invited
    players -= chess.getExcludedPlayers(criteria['exclude'] if 'exclude' in criteria else "")
    exclude_clubs = chess.stringToSet(criteria['exclude-teams'] if 'exclude-teams' in criteria else "")
    players = list(filter(lambda u:chess.isMemberOfAny(u, exclude_clubs), players))
    today = datetime.today().strftime("%Y-%m-%d")
    filename = f"found-players-{today}.txt"
    count = 0
    with open(filename, "a") as pout:
        for u in players:
            pout.write(u)
            pout.write('\n')
            count += 1
        pout.write('\n')
    print(f"..wrote {count} players to {filename}", file=stderr) 

    print(f"..reviewing stats for {count} players", file=stderr)
    eligible = []   # we will use this for sorting by rating
    count = 0
    for u in players:
        count += 1
        if count % 200 == 0:
            print(f"  ..checked {count} players", file=stderr)
        if chess.isMember(u, target_team):
            if verbose:
                print(f"  ..@{u} is member of {target_team}", file=stderr)
            continue
        if chess.matchesStats(u, criteria, verbose=verbose) :
            stats = chess.stats(u)
            # put into list of 3-tuples for sorting
            rating = chess.rating(stats, "chess")
            eligible.append((u, rating, stats))


    print("*** Recruitment candidates for", target_team, "***")
    chess.reportEligiblePlayers(eligible, verbose=verbose)
    # eligible.sort(key=itemgetter(1), reverse=True)
    # for (u, rating, stats) in eligible:
    #     chess.printStats(u, stats)

if (len(sys.argv) < 2):
    print("Usage:", sys.argv[0], "[--verbose] | [--refresh] | [--min/--max rating] [--exclude-current] team [team...]", file=stderr)
else:
    main(sys.argv)

