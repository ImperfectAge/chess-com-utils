#!/usr/bin/env python3

#  https://api.chess.com/pub/player/{username}/stats

import json
import urllib.request
import sys

username = sys.argv[1]
if username[0] == '@':
    username = username[1:]

with urllib.request.urlopen('https://api.chess.com/pub/player/' + username + '/stats',timeout=5) as response:
    members_bytes = response.read()
    member = json.loads(members_bytes.decode('utf8'))
    #members = members_bytes.decode('utf8')

print ('@' + username, '\t',
            'daily:', member['chess_daily']['last']['rating'], '\t',
            '960:',  member['chess960_daily']['last']['rating'], '\t',
            'blitz:',  member['chess_blitz']['last']['rating'])
