#!/usr/bin/env python3

import sys
import chess
from datetime import date
from datetime import datetime
from operator import itemgetter
from sys import stderr

# getMatchInfo: returns some info about the match that caller can do something with
# return players, oteam, settings, start, info
def getMatchInfo(team, matchid, tz="UTC"):
    match = chess.match_details(matchid)
    try:
        teams = match['teams']
        team1 = teams['team1']['name']
        team2 = teams['team2']['name']
        team1url = teams['team1']['url']
        team2url = teams['team2']['url']
        settings = match['settings']
    except Exception as e:
        print("Failed to find details for matchid", matchid, "\n", match, file=stderr)
        return [], None, None, 0, ""

    min_rating = settings.get('min_rating', 0)
    max_rating = settings.get('max_rating', 9999)

    info = ' '.join([match['name'], match['url']])
    if (settings['rules'] != 'chess'):
        info = ' '.join([info, settings['rules']])
    start = match.get('start_time', 0)
    info = ' '.join([info, 'starts:', chess.tzDateTime(start, tz)])
    info = ' '.join([info, 'rating:', chess.describeRating(min_rating, max_rating)])

    if (team == team1) or (team1url.find(team) > -1):
        players = teams['team1']['players']
        oteam = team2url.rpartition('/')[2]
    elif (team == team2) or (team2url.find(team) > -1):
        players = teams['team2']['players']
        oteam = team1url.rpartition('/')[2]
    else:
        print('Neither team matches in',
              match['name'], match['url'], " - looking for team", team, file=stderr)
        return [], None, None, 0, ""
    return players, oteam, settings, start, info


def calcNotSignedUp(members, players, oteam, settings, start, minimum):
    signedup = set()
    for u in players:
        signedup.add(u['username'])

    min_rating = max(settings.get('min_rating', 0), minimum)
    max_rating = settings.get('max_rating', 9999)

    eligible_for_match = []
    for u in members:
        # only look at members who didn't sign up and are not members of the other team
        is_signedup = u in signedup
        is_in_other_team = chess.isMember(u, oteam)
        if verbose:
            if is_signedup:
                print("not-signed-up: @", u, " is already signed up", sep='', file=stderr)
            if is_in_other_team:
                print("not-signed-up: @", u, " is in other team", sep='', file=stderr)

        if not is_signedup and not is_in_other_team:
            # make sure player was a member of the club when the match started
            joined = members[u]
            if start > 0 and joined > start:
                if verbose:
                    print("not-signed-up: @", u, " joined after match start date", sep='', file=stderr)
                continue
            # get the players rating and make sure he was eligible
            ustats = chess.stats(u)
            if not ustats:
                if verbose:
                    print("not-signed-up: @", u, " - could not load stats", sep='', file=stderr)
                continue
            # make sure we get 960 rating if match was 960
            rating = chess.rating(ustats, settings['rules'])
            if (not chess.isRefreshed() and rating > 0 and rating >= min_rating and rating <= max_rating):
                # double-check not now out of range
                ustats = chess.stats(u, refresh=True)
                rating = chess.rating(ustats, settings['rules'])

            to = chess.timeout_ratio(ustats)
            num_games = chess.num_completed_games(ustats, settings['rules'])
            if num_games < 3:
                if verbose:
                    print("not-signed-up: @"+u, "is not eligible due to num games=", num_games, file=stderr)
                continue
            if to >= 30:
                if verbose:
                    print("not-signed-up: @"+u, "is not eligible due to timeout ratio", to, file=stderr)
                continue
            if (rating > 0 and rating >= min_rating and rating <= max_rating):
                # put into list of 3-tuples.
                eligible_for_match.append((u, rating, to))
            else:
                if verbose:
                    print("not-signed-up: @"+u, "is not eligible due to rating", rating, file=stderr)
                continue

    return eligible_for_match


def printEligible(eligible, joinDates, csv=False, minimum=0):
    eligible.sort(key=itemgetter(1), reverse=True)    # sort by rating
    century = 0
    for (u, (rating, max_rating), to) in eligible:
        if rating < minimum:
            break
        details = chess.user_details(u)
        if not details:
            print("not-signed-up: User ", u, "not found!", file=stderr)
        else:
            joined = joinDates[u]     # club join date
            if csv:
                chess.printUserInfoCSV(u, details.get("title", ""), details.get(
                    "name", ""), rating, max_rating, to, joined=joined)
            else:
                if century != int(rating/100):
                    century = int(rating/100)
                    print(f'------------- {rating} - {century*100} -------------------------------------------------')
                chess.printUserInfo(u, details.get("title", ""), details.get(
                    "name", ""), rating, max_rating, to, joined=joined, status=details["status"])

if (len(sys.argv) < 2):
    print(
        "Usage:", sys.argv[0], " [-csv] [-verbose] [-any] [-team <team>] <match-id> [ <match-id> ...]", file=stderr)
    exit(1)

# this is true if we want to list players who could play in ANY of the listed matches
any = False
csv = False
verbose = False
minimum = 0

team = ""
donotdisturb = set()
cfg = chess.readConfig()
if 'home' not in cfg:
    print('No home team defined in config.txt')
    exit(1)

home = cfg['home']
team = home['team']
if 'do_not_disturb' in home:
    donotdisturb = set([name.strip().lower() for name in home['do_not_disturb'].split(',')])

chess.enableCache()
idx = 1
while sys.argv[idx].startswith('-'):
    if sys.argv[idx].startswith('--'):
        sys.argv[idx] = sys.argv[idx][1:]
    if sys.argv[idx] == '-any':
        any = True
    elif sys.argv[idx] == "-csv":
        csv = True
    elif sys.argv[idx] == "-refresh":
        chess.refresh()
    elif sys.argv[idx] == "-team":
        idx += 1
        team = sys.argv[idx]
    elif sys.argv[idx] == "-min" or sys.argv[idx] == "-mininum":
        idx += 1
        minimum = int(sys.argv[idx])
    elif sys.argv[idx] == "-verbose" or sys.argv[idx] == "-v":
        verbose = True
    else:
        print("Unexpected option ", sys.argv[idx], file=stderr)
        exit(2)
    idx += 1

members = chess.membersWithJoinDate(team)

for u in donotdisturb:
    if u in members:
        if verbose:
            print("..removing", "@"+u, "(do-not-disturb)")
        del members[u]

nsu_all = set()      # set for players not-signed-up for any of the matches
nsu_some = set()     # set for players not-signed-up for at least one of the matches
matches = dict()
rules = None

while idx < len(sys.argv):
    matchid = sys.argv[idx]
    idx += 1
    if matchid == ',':
        continue
    if matchid.endswith(","):
        matchid = matchid[:-1]

    players, oteam, settings, start, desc = getMatchInfo(team, matchid)
    
    if rules == None:
        rules = settings['rules']
    elif rules != settings['rules']:
        rules = 'chess'

    print(desc)
    if verbose:
        print(f"\n========== Match: {matchid} - Rules: {rules} ==========\n")

    if rules == "chess960" and 'no_960' in home:
        for u in home['no_960'].split(','):
            u = u.strip().lower()
            if u in members:
                if verbose:
                    print("..removing", "@"+u, "(no 960)")
                del members[u]

    # eligible will be a list of 3-tuples - player, rating, to%
    eligible = calcNotSignedUp(members, players, oteam, settings, start, minimum)

    if verbose:
        print("Following {} players were eligible for this match:".format(len(eligible)))
        eligible.sort(key=itemgetter(1), reverse=True)    # sort by rating
        for (u, rating, to) in eligible:
            if rating < minimum:
                break
            chess.report(u)
        print('\n')

    nsu = set()    # get the usernames only in a set
    for (u, rating, to) in eligible:
        nsu.add(u)

    if len(nsu_some) > 0:
        nsu_all.intersection_update(nsu)      # set intersection operation
    else:
        nsu_all.update(nsu)
    nsu_some.update(nsu)                  # set union operation

    matches[matchid] = nsu

    if verbose:
        print("eligible size=", len(eligible))
        print("nsu size=", len(nsu))
        print("nsu_some size=", len(nsu_some))
        print("nsu_all size=", len(nsu_all))
        print('\n')

if verbose:
    print("\n==============Overall Eligible Report=============\n")

if any:
    # Here we report all players eligible for any of the matches.  We have to indicate which ones
    # Go back to nsu_some which has players who can still sign up for at least one match
    eligible = list()
    for u in nsu_some:
        ustats = chess.stats(u)
        rating = chess.rating(ustats)
        if rating < minimum:
            continue
        to = chess.timeout_ratio(ustats)
        eligible.append((u, rating, to))
    eligible.sort(key=itemgetter(1), reverse=True)    # sort by rating

    if csv:
        chess.printHeaderCSV(matches.keys())

    for (u, rating, to) in eligible:
        if rating < minimum:
            break
        details = chess.user_details(u)
        if not details:
            print("not-signed-up: User ", u, "not found!", file=stderr)
        else:
            joined = members[u]     # club join date
            title = details.get("title", "")
            name = details.get("name", "")
            # Which matches is user not signed up?
            nsu = dict()
            for m in matches:
                nsu[m] = u in matches[m]
            if csv:
                chess.printUserInfoCSV(u, title, name, rating, to, joined=joined, nsu=nsu)
            else:
                chess.printUserInfo(u, title, name, rating, 0, to, joined=joined, nsu=nsu, status=details["status"])

else:
    # Here we want to report only those players not registered in all the matches
    overall = list()
    for u in nsu_all:
        ustats = chess.stats(u)
        rating = chess.rating(ustats, rules)
        if rating < minimum:
            continue
        max_rating = chess.max_rating(ustats)
        to = chess.timeout_ratio(ustats, rules)
        overall.append((u, (rating, max_rating), to))

    # 'overall' will now include only those players eligible for all the matches
    if len(matches) == 1:
        print("\nFollowing {} players were eligible for this match:".format(len(overall)))
    else:
        print("\nFollowing {} players were eligible for these {} matches:".format(len(overall), len(matches)))

    if verbose:
        print("eligible size=", len(overall))
        print("nsu_all size=", len(nsu_all))
        print('\n')

    is960 = '960' in rules
    if (is960):
        print("** using chess960 ratings **")

    # members is passed here only so the called function can get the team join dates
    printEligible(overall, members, csv, minimum)
